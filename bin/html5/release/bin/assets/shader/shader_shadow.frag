precision highp float;

varying vec2 vTextureCoord;
varying vec3 vFragPosition;
varying vec4 vFragPositionLightSpace;
uniform sampler2D uSampler;
uniform sampler2D uNormalSampler;
uniform sampler2D uShadowSampler;

uniform vec3 uLightPos;

varying mat3 TBN;
varying vec3 vNORMAL;

float ShadowCalculation(vec4 fragPositionLightSpace) {
	
	vec3 projCoord = fragPositionLightSpace.xyz / fragPositionLightSpace.w * 0.5 + 0.5;
	float closestDepth = texture2D(uShadowSampler, projCoord.xy).r;
	float currentDepth = projCoord.z;
	vec3 lightDir = normalize( vec3(5, 9, 5) -  vFragPosition);
	float bias = 0.002;//max(0.05*(1.0 - dot(vNORMAL, lightDir)), 0.005);
	
	float shadow = 0.0;
	vec2 texelSize = 1.0/vec2(1024,1024);
	for(int x = -1;x<=1; ++x){
		for(int y=-1; y<=1;++y){
			float pcfDepth = texture2D(uShadowSampler, projCoord.xy + vec2(x, y) * texelSize).r; 
            shadow += currentDepth - bias > pcfDepth  ? 1.0 : 0.0;  
		}
	}
	shadow /= 9.0;
    
    // Keep the shadow at 0.0 when outside the far_plane region of the light's frustum.
    if(projCoord.z > 1.0)
        shadow = 0.0;
        
    return 1.0-shadow;
	/*
	vec3 shadowCoord = (fragPositionLightSpace.xyz/fragPositionLightSpace.w)/2.0 + 0.5;
    vec4 rgbaDepth = texture2D(uShadowSampler, shadowCoord.xy);
    float depth = rgbaDepth.r;
    float visibility = (shadowCoord.z > depth + 0.002) ? 0.2 : 1.0;
	return visibility;
	*/
}


void main(void) {	
	float scale = 10.;	
	
	/**
	vec3 color = texture2D(uSampler, vTextureCoord).rgb;
    vec3 normal = vNORMAL;
    vec3 lightColor = vec3(0.3);
    // ambient
    vec3 ambient = 0.3 * color;
    // diffuse
    vec3 lightDir = normalize(vec3(5.0*scale,9.0*scale,5.0*scale) - vFragPosition);
    float diff = max(dot(lightDir, normal), 0.0);
    vec3 diffuse = diff * color;
    // specular
    vec3 viewDir = normalize(vec3(4.0*scale,40.0*scale,-2.5*scale) - vFragPosition);
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = 0.0;
    vec3 halfwayDir = normalize(lightDir + viewDir);  
    spec = pow(max(dot(normal, halfwayDir), 0.0), 32.0);
    vec3 specular = spec * lightColor;    
    // calculate shadow
    float shadow = ShadowCalculation(vFragPositionLightSpace);                      
    vec3 lighting = (ambient + (1.0 - shadow) * (diffuse + specular)) * color;    
    
    gl_FragColor = vec4(lighting, 1.0);
	*/
	
	
	
	vec3 normal = texture2D(uNormalSampler, vTextureCoord).rgb * 2.0 - 1.0;
    //normal = normalize(normal);
	normal = normalize(TBN * normalize(normal));
	
	vec3 color = texture2D(uSampler, vTextureCoord).rgb;
	vec3 ambient = 0.1*color;
	vec3 lightDir = normalize(vec3(5.*scale, 90.*scale, 5.*scale) - vFragPosition);
	float diff = max(dot(lightDir, normal), 0.0);
	vec3 diffuse = diff * color;
	
	vec3 viewDir = normalize(vec3(4.*scale, 40.*scale, -2.5*scale) - vFragPosition);
	vec3 reflectDir = reflect(-lightDir, normal);
	vec3 halfwayDir = normalize(lightDir + viewDir);
	float spec = pow(max(dot(normal, halfwayDir), 0.0), 64.0);
    vec3 specular = vec3(0.35) * spec;
    float shadow = ShadowCalculation(vFragPositionLightSpace);
    gl_FragColor = vec4(ambient + shadow*(diffuse + specular), 1.0);
}



/*
	vec3 shadowCoord = (vFragPositionLightSpace.xyz/vFragPositionLightSpace.w)/2.0 + 0.5;
	vec4 rgbaDepth = texture2D(uShadowSampler, shadowCoord.xy);
	float depth = rgbaDepth.r;
	float visibility = (shadowCoord.z > depth+0.0015) ? 0.4 : 1.0;
	vec3 environment=vec3(0.5,0.5,0.5);
	vec3 LightColor=vec3(1.0,1.0,1.0);
	vec3 normal=vNORMAL;
	vec3 LightDirection=normalize(vec3(5.*scale, 9.*scale, 5.*scale)-vec3(vFragPosition));
	float nDotL=max(dot(LightDirection,normal),0.0);
	vec3 diffuse;
	vec3 environmentColor;
	diffuse=LightColor*vec3(texture2D(uSampler,vTextureCoord))*nDotL*visibility;
	environmentColor=environment*vec3(texture2D(uSampler,vTextureCoord));
	gl_FragColor =vec4(diffuse+environmentColor, 1.0);


*/