package;


import lime.app.Config;
import lime.utils.AssetLibrary;
import lime.utils.AssetManifest;
import lime.utils.Assets;

#if sys
import sys.FileSystem;
#end

@:access(lime.utils.Assets)


@:keep @:dox(hide) class ManifestResources {
	
	
	public static var preloadLibraries:Array<AssetLibrary>;
	public static var preloadLibraryNames:Array<String>;
	
	
	public static function init (config:Config):Void {
		
		preloadLibraries = new Array ();
		preloadLibraryNames = new Array ();
		
		var rootPath = null;
		
		if (config != null && Reflect.hasField (config, "rootPath")) {
			
			rootPath = Reflect.field (config, "rootPath");
			
		}
		
		if (rootPath == null) {
			
			#if (ios || tvos)
			rootPath = "assets/";
			#elseif (windows && !cs)
			rootPath = FileSystem.absolutePath (haxe.io.Path.directory (#if (haxe_ver >= 3.3) Sys.programPath () #else Sys.executablePath () #end)) + "/";
			#else
			rootPath = "";
			#end
			
		}
		
		Assets.defaultRootPath = rootPath;
		
		#if (openfl && !flash && !display)
		openfl.text.Font.registerFont (__ASSET__OPENFL__assets_font_roboto_medium_ttf);
		openfl.text.Font.registerFont (__ASSET__OPENFL__assets_font_roboto_regular_ttf);
		
		#end
		
		var data, manifest, library;
		
		data = '{"name":null,"assets":"aoy4:sizei137308y4:typey4:FONTy9:classNamey38:__ASSET__assets_font_roboto_medium_ttfy2:idy33:assets%2Ffont%2FRoboto-Medium.ttfy7:preloadtgoy4:pathy34:assets%2Ffont%2FRoboto-Medium.woffR0i94260R1y6:BINARYR5R9R7tgoR0i145348R1R2R3y39:__ASSET__assets_font_roboto_regular_ttfR5y34:assets%2Ffont%2FRoboto-Regular.ttfR7tgoR8y35:assets%2Ffont%2FRoboto-Regular.woffR0i92952R1R10R5R13R7tgoR8y21:assets%2Fimg%2Fbg.pngR0i969435R1y5:IMAGER5R14R7tgoR8y41:assets%2Fimg%2Fcontrol%2FArrowDownBtn.pngR0i1638R1R15R5R16R7tgoR8y40:assets%2Fimg%2Fcontrol%2FArrowForBtn.pngR0i630R1R15R5R17R7tgoR8y41:assets%2Fimg%2Fcontrol%2FArrowOverBtn.pngR0i1617R1R15R5R18R7tgoR8y39:assets%2Fimg%2Fcontrol%2FArrowUpBtn.pngR0i1677R1R15R5R19R7tgoR8y39:assets%2Fimg%2Fcontrol%2FBigBtnDown.pngR0i3604R1R15R5R20R7tgoR8y39:assets%2Fimg%2Fcontrol%2FBigBtnOver.pngR0i3465R1R15R5R21R7tgoR8y37:assets%2Fimg%2Fcontrol%2FBigBtnUp.pngR0i3993R1R15R5R22R7tgoR8y39:assets%2Fimg%2Fcontrol%2FMidBtnDown.pngR0i2664R1R15R5R23R7tgoR8y39:assets%2Fimg%2Fcontrol%2FMidBtnOver.pngR0i2399R1R15R5R24R7tgoR8y37:assets%2Fimg%2Fcontrol%2FMidBtnUp.pngR0i2616R1R15R5R25R7tgoR8y39:assets%2Fimg%2Fcontrol%2FRectangle1.pngR0i58593R1R15R5R26R7tgoR8y39:assets%2Fimg%2Fcontrol%2FRectangle2.pngR0i61616R1R15R5R27R7tgoR8y44:assets%2Fimg%2Fcontrol%2FRectangleBorder.pngR0i25578R1R15R5R28R7tgoR8y40:assets%2Fimg%2Fcontrol%2FRollBtnDown.pngR0i3805R1R15R5R29R7tgoR8y40:assets%2Fimg%2Fcontrol%2FRollBtnOver.pngR0i3791R1R15R5R30R7tgoR8y38:assets%2Fimg%2Fcontrol%2FRollBtnUp.pngR0i3790R1R15R5R31R7tgoR8y34:assets%2Fimg%2Frisk%2FDisabled.pngR0i150R1R15R5R32R7tgoR8y30:assets%2Fimg%2Frisk%2FFive.pngR0i669R1R15R5R33R7tgoR8y30:assets%2Fimg%2Frisk%2FFour.pngR0i572R1R15R5R34R7tgoR8y29:assets%2Fimg%2Frisk%2FOne.pngR0i369R1R15R5R35R7tgoR8y38:assets%2Fimg%2Frisk%2FselectBorder.pngR0i409R1R15R5R36R7tgoR8y29:assets%2Fimg%2Frisk%2FSix.pngR0i931R1R15R5R37R7tgoR8y31:assets%2Fimg%2Frisk%2FThree.pngR0i895R1R15R5R38R7tgoR8y29:assets%2Fimg%2Frisk%2FTwo.pngR0i760R1R15R5R39R7tgoR8y35:assets%2Fimg%2Frisk%2FwinBorder.pngR0i408R1R15R5R40R7tgoR8y33:assets%2Fimg%2Fstats%2Factive.pngR0i1128R1R15R5R41R7tgoR8y35:assets%2Fimg%2Fstats%2Fdeactive.pngR0i926R1R15R5R42R7tgoR8y36:assets%2Fimg%2Fstats%2Fdividehor.pngR0i87R1R15R5R43R7tgoR8y31:assets%2Fimg%2Fstats%2Fmask.pngR0i113R1R15R5R44R7tgoR8y61:assets%2Fimg%2Ftopbar%2Fbutton%2FBlackButtonDisabledAsset.pngR0i2260R1R15R5R45R7tgoR8y57:assets%2Fimg%2Ftopbar%2Fbutton%2FBlackButtonDownAsset.pngR0i1219R1R15R5R46R7tgoR8y57:assets%2Fimg%2Ftopbar%2Fbutton%2FBlackButtonOverAsset.pngR0i2240R1R15R5R47R7tgoR8y55:assets%2Fimg%2Ftopbar%2Fbutton%2FBlackButtonUpAsset.pngR0i2223R1R15R5R48R7tgoR8y64:assets%2Fimg%2Ftopbar%2Fbutton%2FFullScreenIconDisabledAsset.pngR0i1116R1R15R5R49R7tgoR8y60:assets%2Fimg%2Ftopbar%2Fbutton%2FFullScreenIconDownAsset.pngR0i1113R1R15R5R50R7tgoR8y60:assets%2Fimg%2Ftopbar%2Fbutton%2FFullScreenIconOverAsset.pngR0i1335R1R15R5R51R7tgoR8y58:assets%2Fimg%2Ftopbar%2Fbutton%2FFullScreenIconUpAsset.pngR0i1064R1R15R5R52R7tgoR8y61:assets%2Fimg%2Ftopbar%2Fbutton%2FGreenButtonDisabledAsset.pngR0i4274R1R15R5R53R7tgoR8y57:assets%2Fimg%2Ftopbar%2Fbutton%2FGreenButtonDownAsset.pngR0i7838R1R15R5R54R7tgoR8y57:assets%2Fimg%2Ftopbar%2Fbutton%2FGreenButtonOverAsset.pngR0i7627R1R15R5R55R7tgoR8y55:assets%2Fimg%2Ftopbar%2Fbutton%2FGreenButtonUpAsset.pngR0i10372R1R15R5R56R7tgoR8y59:assets%2Fimg%2Ftopbar%2Fbutton%2FRedButtonDisabledAsset.pngR0i4274R1R15R5R57R7tgoR8y55:assets%2Fimg%2Ftopbar%2Fbutton%2FRedButtonDownAsset.pngR0i5769R1R15R5R58R7tgoR8y55:assets%2Fimg%2Ftopbar%2Fbutton%2FRedButtonOverAsset.pngR0i6490R1R15R5R59R7tgoR8y53:assets%2Fimg%2Ftopbar%2Fbutton%2FRedButtonUpAsset.pngR0i6207R1R15R5R60R7tgoR8y63:assets%2Fimg%2Ftopbar%2Fbutton%2FSoundDisabledIconDownAsset.pngR0i491R1R15R5R61R7tgoR8y63:assets%2Fimg%2Ftopbar%2Fbutton%2FSoundDisabledIconOverAsset.pngR0i515R1R15R5R62R7tgoR8y61:assets%2Fimg%2Ftopbar%2Fbutton%2FSoundDisabledIconUpAsset.pngR0i515R1R15R5R63R7tgoR8y62:assets%2Fimg%2Ftopbar%2Fbutton%2FSoundEnabledIconDownAsset.pngR0i492R1R15R5R64R7tgoR8y62:assets%2Fimg%2Ftopbar%2Fbutton%2FSoundEnabledIconOverAsset.pngR0i541R1R15R5R65R7tgoR8y60:assets%2Fimg%2Ftopbar%2Fbutton%2FSoundEnabledIconUpAsset.pngR0i541R1R15R5R66R7tgoR8y47:assets%2Fimg%2Ftopbar%2FtopbarBg%2FtopBarBg.pngR0i89R1R15R5R67R7tgoR8y19:assets%2Findex.htmlR0i2305R1y4:TEXTR5R68R7tgoR8y28:assets%2Flib%2Ffullscreen.jsR0i1119R1R69R5R70R7tgoR8y35:assets%2Flib%2FgetResourceConfig.jsR0i111R1R69R5R71R7tgoR8y23:assets%2Flib%2Fismob.jsR0i424R1R69R5R72R7tgoR8y27:assets%2Flib%2Fismob.min.jsR0i424R1R69R5R73R7tgoR8y26:assets%2Flib%2Fpako.min.jsR0i45411R1R69R5R74R7tgoR8y26:assets%2Fshader%2Fdice.objR0i10357R1R69R5R75R7tgoR8y33:assets%2Fshader%2Fdiffuse_map.pngR0i247118R1R15R5R76R7tgoR8y32:assets%2Fshader%2Fnormal_map.pngR0i159424R1R15R5R77R7tgoR8y31:assets%2Fshader%2Fshader11.fragR0i1665R1R69R5R78R7tgoR8y31:assets%2Fshader%2Fshader11.vertR0i4006R1R69R5R79R7tgoR8y36:assets%2Fshader%2Fshader_shadow.fragR0i4016R1R69R5R80R7tgoR8y36:assets%2Fshader%2Fshader_shadow.vertR0i2937R1R69R5R81R7tgoR8y42:assets%2Fshader%2Fshader_shadow_depth.fragR0i100R1R69R5R82R7tgoR8y42:assets%2Fshader%2Fshader_shadow_depth.vertR0i160R1R69R5R83R7tgoR8y27:assets%2Fshader%2FTable.objR0i130027R1R69R5R84R7tgoR8y36:assets%2Fshader%2FTable1_Diffuse.pngR0i1927347R1R15R5R85R7tgoR8y35:assets%2Fshader%2FTable1_Normal.pngR0i382610R1R15R5R86R7tgoR8y33:assets%2Fshader%2FTable_plane.objR0i12515R1R69R5R87R7tgh","version":2,"libraryArgs":[],"libraryType":null}';
		manifest = AssetManifest.parse (data, rootPath);
		library = AssetLibrary.fromManifest (manifest);
		Assets.registerLibrary ("default", library);
		
		
		library = Assets.getLibrary ("default");
		if (library != null) preloadLibraries.push (library);
		else preloadLibraryNames.push ("default");
		
		
	}
	
	
}


#if !display
#if flash

@:keep @:bind #if display private #end class __ASSET__assets_font_roboto_medium_ttf extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_font_roboto_medium_woff extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_font_roboto_regular_ttf extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_font_roboto_regular_woff extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_img_bg_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_control_arrowdownbtn_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_control_arrowforbtn_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_control_arrowoverbtn_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_control_arrowupbtn_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_control_bigbtndown_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_control_bigbtnover_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_control_bigbtnup_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_control_midbtndown_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_control_midbtnover_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_control_midbtnup_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_control_rectangle1_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_control_rectangle2_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_control_rectangleborder_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_control_rollbtndown_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_control_rollbtnover_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_control_rollbtnup_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_risk_disabled_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_risk_five_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_risk_four_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_risk_one_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_risk_selectborder_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_risk_six_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_risk_three_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_risk_two_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_risk_winborder_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_stats_active_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_stats_deactive_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_stats_dividehor_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_stats_mask_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_blackbuttondisabledasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_blackbuttondownasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_blackbuttonoverasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_blackbuttonupasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_fullscreenicondisabledasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_fullscreenicondownasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_fullscreeniconoverasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_fullscreeniconupasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_greenbuttondisabledasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_greenbuttondownasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_greenbuttonoverasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_greenbuttonupasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_redbuttondisabledasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_redbuttondownasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_redbuttonoverasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_redbuttonupasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_sounddisabledicondownasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_sounddisablediconoverasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_sounddisablediconupasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_soundenabledicondownasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_soundenablediconoverasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_button_soundenablediconupasset_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_img_topbar_topbarbg_topbarbg_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_index_html extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_lib_fullscreen_js extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_lib_getresourceconfig_js extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_lib_ismob_js extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_lib_ismob_min_js extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_lib_pako_min_js extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_shader_dice_obj extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_shader_diffuse_map_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_shader_normal_map_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_shader_shader11_frag extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_shader_shader11_vert extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_shader_shader_shadow_frag extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_shader_shader_shadow_vert extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_shader_shader_shadow_depth_frag extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_shader_shader_shadow_depth_vert extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_shader_table_obj extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_shader_table1_diffuse_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_shader_table1_normal_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }@:keep @:bind #if display private #end class __ASSET__assets_shader_table_plane_obj extends null { }
@:keep @:bind #if display private #end class __ASSET__manifest_default_json extends null { }


#elseif (desktop || cpp)

@:font("bin/html5/release/obj/webfont/Roboto-Medium.ttf") #if display private #end class __ASSET__assets_font_roboto_medium_ttf extends lime.text.Font {}
@:file("assets/font/Roboto-Medium.woff") #if display private #end class __ASSET__assets_font_roboto_medium_woff extends haxe.io.Bytes {}
@:font("bin/html5/release/obj/webfont/Roboto-Regular.ttf") #if display private #end class __ASSET__assets_font_roboto_regular_ttf extends lime.text.Font {}
@:file("assets/font/Roboto-Regular.woff") #if display private #end class __ASSET__assets_font_roboto_regular_woff extends haxe.io.Bytes {}
@:image("assets/img/bg.png") #if display private #end class __ASSET__assets_img_bg_png extends lime.graphics.Image {}
@:image("assets/img/control/ArrowDownBtn.png") #if display private #end class __ASSET__assets_img_control_arrowdownbtn_png extends lime.graphics.Image {}
@:image("assets/img/control/ArrowForBtn.png") #if display private #end class __ASSET__assets_img_control_arrowforbtn_png extends lime.graphics.Image {}
@:image("assets/img/control/ArrowOverBtn.png") #if display private #end class __ASSET__assets_img_control_arrowoverbtn_png extends lime.graphics.Image {}
@:image("assets/img/control/ArrowUpBtn.png") #if display private #end class __ASSET__assets_img_control_arrowupbtn_png extends lime.graphics.Image {}
@:image("assets/img/control/BigBtnDown.png") #if display private #end class __ASSET__assets_img_control_bigbtndown_png extends lime.graphics.Image {}
@:image("assets/img/control/BigBtnOver.png") #if display private #end class __ASSET__assets_img_control_bigbtnover_png extends lime.graphics.Image {}
@:image("assets/img/control/BigBtnUp.png") #if display private #end class __ASSET__assets_img_control_bigbtnup_png extends lime.graphics.Image {}
@:image("assets/img/control/MidBtnDown.png") #if display private #end class __ASSET__assets_img_control_midbtndown_png extends lime.graphics.Image {}
@:image("assets/img/control/MidBtnOver.png") #if display private #end class __ASSET__assets_img_control_midbtnover_png extends lime.graphics.Image {}
@:image("assets/img/control/MidBtnUp.png") #if display private #end class __ASSET__assets_img_control_midbtnup_png extends lime.graphics.Image {}
@:image("assets/img/control/Rectangle1.png") #if display private #end class __ASSET__assets_img_control_rectangle1_png extends lime.graphics.Image {}
@:image("assets/img/control/Rectangle2.png") #if display private #end class __ASSET__assets_img_control_rectangle2_png extends lime.graphics.Image {}
@:image("assets/img/control/RectangleBorder.png") #if display private #end class __ASSET__assets_img_control_rectangleborder_png extends lime.graphics.Image {}
@:image("assets/img/control/RollBtnDown.png") #if display private #end class __ASSET__assets_img_control_rollbtndown_png extends lime.graphics.Image {}
@:image("assets/img/control/RollBtnOver.png") #if display private #end class __ASSET__assets_img_control_rollbtnover_png extends lime.graphics.Image {}
@:image("assets/img/control/RollBtnUp.png") #if display private #end class __ASSET__assets_img_control_rollbtnup_png extends lime.graphics.Image {}
@:image("assets/img/risk/Disabled.png") #if display private #end class __ASSET__assets_img_risk_disabled_png extends lime.graphics.Image {}
@:image("assets/img/risk/Five.png") #if display private #end class __ASSET__assets_img_risk_five_png extends lime.graphics.Image {}
@:image("assets/img/risk/Four.png") #if display private #end class __ASSET__assets_img_risk_four_png extends lime.graphics.Image {}
@:image("assets/img/risk/One.png") #if display private #end class __ASSET__assets_img_risk_one_png extends lime.graphics.Image {}
@:image("assets/img/risk/selectBorder.png") #if display private #end class __ASSET__assets_img_risk_selectborder_png extends lime.graphics.Image {}
@:image("assets/img/risk/Six.png") #if display private #end class __ASSET__assets_img_risk_six_png extends lime.graphics.Image {}
@:image("assets/img/risk/Three.png") #if display private #end class __ASSET__assets_img_risk_three_png extends lime.graphics.Image {}
@:image("assets/img/risk/Two.png") #if display private #end class __ASSET__assets_img_risk_two_png extends lime.graphics.Image {}
@:image("assets/img/risk/winBorder.png") #if display private #end class __ASSET__assets_img_risk_winborder_png extends lime.graphics.Image {}
@:image("assets/img/stats/active.png") #if display private #end class __ASSET__assets_img_stats_active_png extends lime.graphics.Image {}
@:image("assets/img/stats/deactive.png") #if display private #end class __ASSET__assets_img_stats_deactive_png extends lime.graphics.Image {}
@:image("assets/img/stats/dividehor.png") #if display private #end class __ASSET__assets_img_stats_dividehor_png extends lime.graphics.Image {}
@:image("assets/img/stats/mask.png") #if display private #end class __ASSET__assets_img_stats_mask_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/BlackButtonDisabledAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_blackbuttondisabledasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/BlackButtonDownAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_blackbuttondownasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/BlackButtonOverAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_blackbuttonoverasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/BlackButtonUpAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_blackbuttonupasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/FullScreenIconDisabledAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_fullscreenicondisabledasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/FullScreenIconDownAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_fullscreenicondownasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/FullScreenIconOverAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_fullscreeniconoverasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/FullScreenIconUpAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_fullscreeniconupasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/GreenButtonDisabledAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_greenbuttondisabledasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/GreenButtonDownAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_greenbuttondownasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/GreenButtonOverAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_greenbuttonoverasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/GreenButtonUpAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_greenbuttonupasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/RedButtonDisabledAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_redbuttondisabledasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/RedButtonDownAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_redbuttondownasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/RedButtonOverAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_redbuttonoverasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/RedButtonUpAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_redbuttonupasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/SoundDisabledIconDownAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_sounddisabledicondownasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/SoundDisabledIconOverAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_sounddisablediconoverasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/SoundDisabledIconUpAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_sounddisablediconupasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/SoundEnabledIconDownAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_soundenabledicondownasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/SoundEnabledIconOverAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_soundenablediconoverasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/button/SoundEnabledIconUpAsset.png") #if display private #end class __ASSET__assets_img_topbar_button_soundenablediconupasset_png extends lime.graphics.Image {}
@:image("assets/img/topbar/topbarBg/topBarBg.png") #if display private #end class __ASSET__assets_img_topbar_topbarbg_topbarbg_png extends lime.graphics.Image {}
@:file("assets/index.html") #if display private #end class __ASSET__assets_index_html extends haxe.io.Bytes {}
@:file("assets/lib/fullscreen.js") #if display private #end class __ASSET__assets_lib_fullscreen_js extends haxe.io.Bytes {}
@:file("assets/lib/getResourceConfig.js") #if display private #end class __ASSET__assets_lib_getresourceconfig_js extends haxe.io.Bytes {}
@:file("assets/lib/ismob.js") #if display private #end class __ASSET__assets_lib_ismob_js extends haxe.io.Bytes {}
@:file("assets/lib/ismob.min.js") #if display private #end class __ASSET__assets_lib_ismob_min_js extends haxe.io.Bytes {}
@:file("assets/lib/pako.min.js") #if display private #end class __ASSET__assets_lib_pako_min_js extends haxe.io.Bytes {}
@:file("assets/shader/dice.obj") #if display private #end class __ASSET__assets_shader_dice_obj extends haxe.io.Bytes {}
@:image("assets/shader/diffuse_map.png") #if display private #end class __ASSET__assets_shader_diffuse_map_png extends lime.graphics.Image {}
@:image("assets/shader/normal_map.png") #if display private #end class __ASSET__assets_shader_normal_map_png extends lime.graphics.Image {}
@:file("assets/shader/shader11.frag") #if display private #end class __ASSET__assets_shader_shader11_frag extends haxe.io.Bytes {}
@:file("assets/shader/shader11.vert") #if display private #end class __ASSET__assets_shader_shader11_vert extends haxe.io.Bytes {}
@:file("assets/shader/shader_shadow.frag") #if display private #end class __ASSET__assets_shader_shader_shadow_frag extends haxe.io.Bytes {}
@:file("assets/shader/shader_shadow.vert") #if display private #end class __ASSET__assets_shader_shader_shadow_vert extends haxe.io.Bytes {}
@:file("assets/shader/shader_shadow_depth.frag") #if display private #end class __ASSET__assets_shader_shader_shadow_depth_frag extends haxe.io.Bytes {}
@:file("assets/shader/shader_shadow_depth.vert") #if display private #end class __ASSET__assets_shader_shader_shadow_depth_vert extends haxe.io.Bytes {}
@:file("assets/shader/Table.obj") #if display private #end class __ASSET__assets_shader_table_obj extends haxe.io.Bytes {}
@:image("assets/shader/Table1_Diffuse.png") #if display private #end class __ASSET__assets_shader_table1_diffuse_png extends lime.graphics.Image {}
@:image("assets/shader/Table1_Normal.png") #if display private #end class __ASSET__assets_shader_table1_normal_png extends lime.graphics.Image {}
@:file("assets/shader/Table_plane.obj") #if display private #end class __ASSET__assets_shader_table_plane_obj extends haxe.io.Bytes {}
@:file("") #if display private #end class __ASSET__manifest_default_json extends haxe.io.Bytes {}



#else

@:keep @:expose('__ASSET__assets_font_roboto_medium_ttf') #if display private #end class __ASSET__assets_font_roboto_medium_ttf extends lime.text.Font { public function new () { #if !html5 __fontPath = "assets/font/Roboto-Medium"; #end name = "Roboto Medium"; super (); }}
@:keep @:expose('__ASSET__assets_font_roboto_regular_ttf') #if display private #end class __ASSET__assets_font_roboto_regular_ttf extends lime.text.Font { public function new () { #if !html5 __fontPath = "assets/font/Roboto-Regular"; #end name = "Roboto Regular"; super (); }}


#end

#if (openfl && !flash)

@:keep @:expose('__ASSET__OPENFL__assets_font_roboto_medium_ttf') #if display private #end class __ASSET__OPENFL__assets_font_roboto_medium_ttf extends openfl.text.Font { public function new () { var font = new __ASSET__assets_font_roboto_medium_ttf (); src = font.src; name = font.name; super (); }}
@:keep @:expose('__ASSET__OPENFL__assets_font_roboto_regular_ttf') #if display private #end class __ASSET__OPENFL__assets_font_roboto_regular_ttf extends openfl.text.Font { public function new () { var font = new __ASSET__assets_font_roboto_regular_ttf (); src = font.src; name = font.name; super (); }}


#end
#end