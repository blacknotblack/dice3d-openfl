package;

import controller.RECEIVED_PACKET_COMMAND;
import controller.RESOURCE_LOAD_COMMAND;
import controller.SENDING_PACKET_COMMAND;
import controller.STARTUP_COMPLETE_COMMAND;
import controller.viewCommands.SCREEN_BUTTON_TRIGGERED_COMMAND;
import events.ApplicationEvent;
import js.Browser;
import js.html.CanvasElement;
import openfl.display.Sprite;
import openfl.Lib;
import openfl.text.TextField;
import openfl.text.TextFormat;
import service.socket.SocketService;
import view.gamelayer.GameLayer;
import view.gameLayer.buttonsLayer.gameTypeButton.button.BaseButton;
import view.riskLayer.RiskLayer;
import view.topbar.TopBar;

/**
 * ...
 * @author Igor Skotnikov
 */
class Main extends Sprite 
{

	public static var WIDTH:Float = 1920;
	public static var HEIGHT:Float = 1080;
	
	var topBar:TopBar;
	var gameLayer:GameLayer;
	var riskLayer:RiskLayer;
	var context:Context;
	var commands:Array<Dynamic>;
	
	public function new() 
	{
		super();
		configureContext();
		GlobalEventDispatcher.getInstance().eventDispatcher.addEventListener(ApplicationEvent.ASSETS_LOADED, function (e:ApplicationEvent) {
			//addChild(riskLayer = new RiskLayer());
			GlobalEventDispatcher.getInstance().dispatch(new ApplicationEvent(ApplicationEvent.RESOURCE_LOAD));
			addChild(topBar = new TopBar());
			addChild(gameLayer = new GameLayer());
			
			/*
			var c:CanvasElement =  cast Browser.document.getElementsByTagName('canvas')[0];
			var txt:TextField = new TextField();
			txt.x = 100;
			txt.y = 100;
			txt.textColor = 0xffffff;
			var _format:TextFormat = new TextFormat();
			_format.size = 30;
			txt.defaultTextFormat = _format;
			txt.text = ""+Browser.window.innerWidth;
			addChild(txt);
			*/
		});
		GameAssets.getInstance().init();
		//addChild(new GameLayer());
		//addChild(new TopBar());
	}
	
	private function configureContext():Void {
		context = new Context(this);
		commands = [];
		commands.push(new SENDING_PACKET_COMMAND());
		commands.push(new RECEIVED_PACKET_COMMAND());
		commands.push(new SCREEN_BUTTON_TRIGGERED_COMMAND());
		commands.push(new STARTUP_COMPLETE_COMMAND());
		commands.push(new RESOURCE_LOAD_COMMAND());
		context.socketService = new SocketService();
	}
}
