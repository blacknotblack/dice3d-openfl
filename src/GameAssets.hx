package;

import events.ApplicationEvent;
import js.html.XMLHttpRequest;
import openfl.display.BitmapData;
import openfl.errors.Error;
import openfl.events.Event;
import openfl.text.Font;
import openfl.Assets;
import view.gameLayer.tableLayer.ObjLoader;

/**
 * ...
 * @author Igor Skotnikov
 */
class GameAssets
{
	private static var _instance:GameAssets = new GameAssets();
	public var font(default, null):Font;
	public var fontRobotoRegular(default, null):Font;
	public var fontPlumbBlack(default, null):Font;
	
	//3d
	public var objLoaderTable:ObjLoader;
	public var objLoaderDice:ObjLoader;
	
	// img
	public var NUMBER_FIELD:BitmapData;
	public var BET_FIELD:BitmapData;
	
	//bg
	public var BACKGROUND: BitmapData;
	
	//risc
	public var RISK_DICE1:BitmapData;
	public var RISK_DICE2:BitmapData;
	public var RISK_DICE3:BitmapData;
	public var RISK_DICE4:BitmapData;
	public var RISK_DICE5:BitmapData;
	public var RISK_DICE6:BitmapData;
	public var RISK_DICE_DISABLED:BitmapData;
	public var RISC_DICE_SELECT_BORDER:BitmapData;
	public var RISC_DICE_WIN_BORDER:BitmapData;
	
	//topbar
	public var TOP_BAR_BG:BitmapData;
	//popUp
	public var TITLE_POPUP:BitmapData;
	public var COVER_POPUP:BitmapData;
	public var CONTENT_TOP_POPUP:BitmapData;
	public var CONTENT_BOTTOM_POPUP:BitmapData;
	
	//stats
	public var DIVIDER:BitmapData;
	
	public function new() 
	{
		if (_instance!=null) throw new Error("Singleton and can only be accessed through Singleton.getInstance()");
	}
	
	public function init():Void {
		fontRobotoRegular = Assets.getFont("assets/font/Roboto-Regular.ttf");
		//3d
		var xml:XMLHttpRequest = new XMLHttpRequest();
		xml.open("GET", "assets/shader/Table.obj", false);
		xml.onload = function () {
			objLoaderTable = new ObjLoader(xml.responseText);
			for (i in 0...objLoaderTable.data.length) {
				if (objLoaderTable.data[i] == null) {
					objLoaderTable.data = objLoaderTable.data.slice(0, i);
					break;
				}
			}
		}
		xml.send();		
		xml.open("GET", "assets/shader/dice.obj", false);
		xml.onload = function () {
			objLoaderDice = new ObjLoader(xml.responseText);
			for (i in 0...objLoaderDice.data.length) {
				if (objLoaderDice.data[i] == null) {
					objLoaderDice.data = objLoaderDice.data.slice(0, i);
					break;
				}
			}
		}
		xml.send();
			
		NUMBER_FIELD = Assets.getBitmapData("assets/img/control/Rectangle1.png");
		BET_FIELD = Assets.getBitmapData("assets/img/control/Rectangle2.png");
		
		BACKGROUND = Assets.getBitmapData("assets/img/bg.png");
		
		RISK_DICE1 = Assets.getBitmapData("assets/img/risk/One.png");
		RISK_DICE2 = Assets.getBitmapData("assets/img/risk/Two.png");
		RISK_DICE3 = Assets.getBitmapData("assets/img/risk/Three.png");
		RISK_DICE4 = Assets.getBitmapData("assets/img/risk/Four.png");
		RISK_DICE5 = Assets.getBitmapData("assets/img/risk/Five.png");
		RISK_DICE6 = Assets.getBitmapData("assets/img/risk/Six.png");
		RISK_DICE_DISABLED = Assets.getBitmapData("assets/img/risk/Disabled.png");
		RISC_DICE_SELECT_BORDER = Assets.getBitmapData("assets/img/risk/selectBorder.png");
		RISC_DICE_WIN_BORDER = Assets.getBitmapData("assets/img/risk/winBorder.png");
		
		DIVIDER = Assets.getBitmapData("assets/img/stats/dividehor.png");
		
		Assets.loadFont("assets/font/Roboto-Medium.ttf").onComplete(function(_font:Font) {
			font = _font;
			GlobalEventDispatcher.getInstance().dispatch(new ApplicationEvent(ApplicationEvent.ASSETS_LOADED));
		});
	}
	
	public static function getInstance():GameAssets {
		return _instance;
	}
}