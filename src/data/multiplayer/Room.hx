package data.multiplayer;
import data.multiplayer.Player;
import openfl.errors.Error;

/**
 * ...
 * @author Igor Skotnikov
 */
class Room
{
	private static var _instance:Room = new Room();

	//public var gameId(default, null):UInt;
	public var player(default, null):Player;
	public var bet(default, null):Float;
	public var multiplicator(default, null):Float = 1.01;
	public var minBet(default, null):Float = 0.0;
	public var maxBet(default, null):Float = 10.0;

	public function new() 
	{
		if (_instance != null)  throw new Error("Must be called throw getInstance()");
		player = new Player();
	}
	
	public function createRoom(minBet:Float, maxBet:Float, multiplicator:Float):Void {
		this.minBet = this.bet = minBet;
		this.maxBet = maxBet;
		this.multiplicator = multiplicator;
	}
	
	public function setBet(bet:Float):Void {
		this.bet = bet;
	}
	
	public static function getInstance():Room {
		return _instance;
	}
	
}