package data.multiplayer;

import openfl.errors.Error;

/**
 * ...
 * @author Igor Skotnikov
 */
class NumberFieldModel
{
	private static var _instance = new NumberFieldModel();
	
	private var over:Array<String> 	= ["2", "3", "4", "5", "6", "7", "8", "9", "10", "11"];
	private var under:Array<String> = ["3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
	private var equal:Array<String> = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
	private var double:Array<String> = ["1:1", "2:2", "3:3", "4:4", "5:5", "6:6"];
	private var all:Array<String> = ["Any Double"];
	
	public var number(default, null):Int;
	
	public function new() 
	{
		if (_instance != null)  throw new Error("Must be called throw getInstance()");
	}
	
	public static function getInstance():NumberFieldModel {
		return _instance;
	}
	
	public function getCurrentModel(type:Int):Array<String> {
		var current:Array<String> = [];
		switch(type) {
			case 1:	current = over.copy();
			case 2:	current = under.copy();
			case 3:	current = equal.copy();
			case 4:	current = double.copy();
			case 5:	current = all.copy();
		}
		return current;
	}
	
	public function setCurrentNumber(string:String):Void {
		number = Std.parseInt(string);
	}
}