package data.multiplayer;

/**
 * ...
 * @author Igor Skotnikov
 */
class Player
{
	public var id:Int;
	public var name:String;
	public var rating:Float;
	public var balance:Float;
	
	/* CONSTRUCTOR */
	public function new() {
		
	}
	
	/* API */
	public function createPlayer(id:Int = 0, name:String="", rating:Float = 0, balance:Float = 0):Void{
		this.id = id;
		this.name = name;
		this.rating = rating;
		this.balance = balance;
	}
	
}