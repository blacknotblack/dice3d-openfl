package data.multiplayer;
import openfl.errors.Error;

/**
 * ...
 * @author Igor Skotnikov
 */
class GameModel
{
	private static var _instance:GameModel = new GameModel();
	
	inline public static var OVER			:Int 	= 1;
	inline public static var UNDER			:Int 	= 2;
	inline public static var EQUAL			:Int 	= 3;
	inline public static var DOUBLE			:Int 	= 4;
	inline public static var ALLDOUBLES		:Int 	= 5;
	
	public var gameType(default, null):Int;
	public function new() 
	{
		if (_instance != null)  throw new Error("Must be called throw getInstance()");
		gameType = 10;
	}
	
	public function setGameType(type:Int):Void {
		gameType = type;
	}
	public function initGameType(type:UInt):Void {
		if (type < gameType) gameType = type;
	}
	
	public static function getInstance():GameModel {
		return _instance;
	}
}