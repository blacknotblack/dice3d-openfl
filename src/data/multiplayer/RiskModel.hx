package data.multiplayer;
import openfl.errors.Error;

/**
 * ...
 * @author Igor Skotnikov
 */
class RiskModel
{
	private static var _instance = new RiskModel();
	
	public var dices(default, null):Array<Int>; 
	
	public function new() 
	{
		if (_instance != null)  throw new Error("Must be called throw getInstance()");
		dices = [];
	}
	
	public function addDice(value:Int):Void {
		dices.push(value);
	}
	
	public function removeDice(value:Int):Void {
		dices.remove(value);
	}
	
	public static function getInstance():RiskModel {
		return _instance;
	}
	
}