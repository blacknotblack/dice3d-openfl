package data.manager.timeoutManager;

class TimeoutData{
	/* VARS */
	public var startTime(default, null)			:Int;
	public var duration(default, null)			:Int;
	public var completeFunction(default, null)	:Dynamic;
	public var completeParams(default, null)	:Array<Dynamic>;
		/* CONSTRUCTOR */
	public function new(startTime:Int, duration:Int, completeFunction:Dynamic, completeParams:Array<Dynamic>=null){
		this.startTime			= startTime;
		this.duration			= duration;
		this.completeFunction 	= completeFunction;
		this.completeParams 	= completeParams;
	}
}
