package data.const;

/**
 * ...
 * @author Igor Skotnikov
 */
class AppCommands
{	
	inline public static var ERROR_ALREADY_PLAYS:UInt 					= 1;	
	inline public static var ERROR_NOT_REGISTERED:UInt 					= 2;
	inline public static var ERROR_BANNED:UInt 							= 3;
	inline public static var ERROR_NO_MONEY:UInt						= 4;
	inline public static var ERROR_WRONG_DATA:UInt						= 7;
	inline public static var ERROR_GAME_BLOCKED:UInt					= 8;
	
	inline public static var SYSTEM_PING:UInt							= 45;

	inline public static var MULTIPLAYER_PLAYER_ADD:UInt				= 100;
	inline public static var AUTOROLL_MAKE_BET_TIMER:UInt				= 101;
	inline public static var AUTOROLL_MADE_BET_TIMER:UInt				= 102;
	inline public static var MULTIPLAYER_ROLL_DICE:UInt 				= 103;
	inline public static var MULTIPLAYER_CASINO_DICE:UInt				= 104;
	inline public static var MULTIPLAYER_ROLL_RISK:UInt					= 105;
	inline public static var MULTIPLAYER_CASINO_RISK:UInt				= 106;
	inline public static var MULTIPLAYER_ALL_BETS_INFO:UInt				= 107;
	inline public static var MULTIPLAYER_LEAVE_ROOM:UInt				= 108;

	
	public function new() {	}
	
}