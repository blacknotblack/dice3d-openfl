package view.gamelayer;

import openfl.events.EventDispatcher;
import openfl.display.Sprite;
import openfl.events.Event;


/**
 * ...
 * @author Igor Skotnikov
 */
class GameLayerMediator
{
	/* VARS */
	private var gameLayer:GameLayer;
	
	
	//-- inject --//
	//private var room:Room;
	private var eventDispatcher:EventDispatcher;
	private var contextview:Sprite;
	
	/* CONSTRUCTOR */
	public function new(gameLayer:GameLayer) 
	{
		this.gameLayer = gameLayer;
		
		//room = Room.getInstance();
		contextview = Context.getInstance().contextview;
		
		setupListeners();
		RESIZE_HANDLER(null);
	}
	
	private function setupListeners():Void {
		eventDispatcher = GlobalEventDispatcher.getInstance().eventDispatcher;
		
		contextview.stage.addEventListener(Event.RESIZE, RESIZE_HANDLER);
	}
	
	/* HANDLERS */
	
	
	private function RESIZE_HANDLER(e:Event):Void {
		
		gameLayer.resize(contextview.stage.stageWidth, contextview.stage.stageHeight);
	}
	
	
	/* API */
	
}