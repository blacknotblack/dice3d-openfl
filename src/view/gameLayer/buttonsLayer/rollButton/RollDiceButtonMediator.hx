package view.gameLayer.buttonsLayer.rollButton;

import openfl.events.Event;
import openfl.events.MouseEvent;
import openfl.events.TouchEvent;
import service.socket.SocketServiceEvent;
import data.const.AppCommands;
import data.multiplayer.GameModel;
import data.multiplayer.NumberFieldModel;
import data.multiplayer.Room;
import view.gameLayer.buttonsLayer.rollButton.RollDiceButton;

/**
 * ...
 * @author Igor Skotnikov
 */
class RollDiceButtonMediator
{
	
	private var view:RollDiceButton;
	public function new(view:RollDiceButton) 
	{
		this.view = view;
			
		if (untyped __js__("ismob()")) {
			view.addEventListener(TouchEvent.TOUCH_BEGIN, MOUSE_HANDLER);
			view.addEventListener(TouchEvent.TOUCH_END, MOUSE_HANDLER);
		}
		else{
			view.addEventListener(MouseEvent.MOUSE_DOWN, MOUSE_HANDLER);
			view.addEventListener(MouseEvent.MOUSE_OVER, MOUSE_HANDLER);
			view.addEventListener(MouseEvent.MOUSE_OUT,  MOUSE_HANDLER);
			view.addEventListener(MouseEvent.MOUSE_UP,  MOUSE_HANDLER);
		}
	}
	
	private function MOUSE_HANDLER(e:Event) {
		switch(e.type) {
			case MouseEvent.MOUSE_DOWN | TouchEvent.TOUCH_BEGIN: 
				GlobalEventDispatcher.getInstance().dispatch(new SocketServiceEvent(SocketServiceEvent.SENDING_PACKET, AppCommands.MULTIPLAYER_ROLL_DICE, {"gameType":GameModel.getInstance().gameType, "dice":NumberFieldModel.getInstance().number, "bet":Room.getInstance().bet} ));
				view.switchState("down");
			case MouseEvent.MOUSE_OVER:
				view.switchState("over");
			case MouseEvent.MOUSE_OUT | MouseEvent.MOUSE_UP | TouchEvent.TOUCH_END:
				view.switchState("out");
		}	
	}
}