package view.gameLayer.buttonsLayer.rollButton;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.Assets;

/**
 * ...
 * @author Igor Skotnikov
 */
class RollDiceButton extends Sprite
{
	
	private var mediator:RollDiceButtonMediator;
	
	private var up:Bitmap;
	private var	over:Bitmap;
	private var down:Bitmap;
	private var tf:TextField;
	
	public function new()
	{
		super();
		
		mediator = new RollDiceButtonMediator(this);
		up = new Bitmap(Assets.getBitmapData("assets/img/control/RollBtnUp.png"), null, true);
		down = new Bitmap(Assets.getBitmapData("assets/img/control/RollBtnDown.png"), null, true);
		over = new Bitmap(Assets.getBitmapData("assets/img/control/RollBtnOver.png"), null, true);
		
		addChild(up);
		
		tf 						= new TextField();
		tf.defaultTextFormat	= new TextFormat(GameAssets.getInstance().fontRobotoRegular.fontName, 18, 0xffffff, null, null, null, null, null, "center");
		tf.embedFonts 			= true;
		tf.selectable 			= false;
		tf.multiline 			= false;
		tf.wordWrap 			= false;
		tf.width				= up.width - 30;
		tf.height				= 40;
		tf.text					= Localization.getInstance().data.rollButtonLabel;
		
		var bmd:BitmapData = new BitmapData(Std.int(tf.width), Std.int(tf.height), false, 0xfff);
		bmd.draw(tf);
		var top:Int = topPoint(bmd);
		var bottom:Int = 40-bottomPoint(bmd);

		tf.y = (up.height - tf.height - top+bottom) / 2+2;
		tf.x = (up.width - tf.width) / 2;
		addChild(tf);
	}
	
	public function switchState(state:String):Void {
		removeChildAt(0);
		switch(state) {
			case "up": 		addChildAt(up, 0);
			case "down":	addChildAt(down, 0);
			case "over":	addChildAt(over, 0); 
			case "out":		addChildAt(up, 0);
		}
	}
	
	private function topPoint(bmd:BitmapData):Int {
		for (i in 0...40)
			for (j in 0...100)
				if (bmd.getPixel(j, i) == 0xffffff) return i;
		return 0;
	}
	private function bottomPoint(bmd:BitmapData):Int {
		for (i in 0...bmd.height)
			for (j in 0...100)
				if (bmd.getPixel(j, bmd.height-i) == 0xffffff) return bmd.height-i;
		return bmd.height;
	}
	
}