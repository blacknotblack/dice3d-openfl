package view.gameLayer.buttonsLayer;
import data.multiplayer.GameModel;
import data.multiplayer.NumberFieldModel;
import openfl.display.Sprite;
import view.gameLayer.buttonsLayer.ButtonsLayerMediator;
import view.gameLayer.buttonsLayer.component.betfield.BetField;
import view.gameLayer.buttonsLayer.component.EOperation;
import view.gameLayer.buttonsLayer.gameTypeButton.button.AllDoubleButton;
import view.gameLayer.buttonsLayer.gameTypeButton.button.DoubleButton;
import view.gameLayer.buttonsLayer.gameTypeButton.button.EqualButton;
import view.gameLayer.buttonsLayer.gameTypeButton.button.OverButton;
import view.gameLayer.buttonsLayer.gameTypeButton.button.UnderButton;
import view.gameLayer.buttonsLayer.gameTypeButton.button.BaseButton;
import view.gameLayer.buttonsLayer.component.numberfield.NumberField;
import view.gameLayer.buttonsLayer.component.numberfield.UpButton;
import view.gameLayer.buttonsLayer.component.numberfield.DownButton;
import view.gameLayer.buttonsLayer.rollButton.RollDiceButton;

/**
 * ...
 * @author Igor Skotnikov
 */
class ButtonsLayer extends Sprite
{
	private var mediator:ButtonsLayerMediator;
	
	private var overButton: OverButton;
	private var underButton: UnderButton;
	private var equalButton: EqualButton;
	private var doubleButtons: Array<DoubleButton>;
	private var allDoubleButton: AllDoubleButton;
	
	private var numberField:NumberField;
	
	private var upButton:UpButton;
	private var downButton:DownButton;
	
	private var betField:BetField;
	private var rollButton:RollDiceButton;
	
	public function new() 
	{
		super();
		
		mediator = new ButtonsLayerMediator(this);
		
		if (AppData.getInstance().over == 1) {
			overButton = new OverButton();
			addChild(overButton);
			GameModel.getInstance().initGameType(1);
		}
		if (AppData.getInstance().under == 1) {
			underButton = new UnderButton();
			underButton.y = numChildren * 55;
			addChild(underButton);
			GameModel.getInstance().initGameType(2);
		}
		if (AppData.getInstance().equal == 1) {
			equalButton = new EqualButton();
			equalButton.y = numChildren * 55;
			addChild(equalButton);
			GameModel.getInstance().initGameType(3);
		}
		if (AppData.getInstance().alldoubles == 1) {
			allDoubleButton = new AllDoubleButton();
			allDoubleButton.y = numChildren * 55;
			addChild(allDoubleButton);
			GameModel.getInstance().initGameType(5);
		}
		var btnsCount:Int = numChildren-1;
		if (AppData.getInstance().double == 1) {
			doubleButtons = [];
			for(i in 1...7){
				doubleButtons[i-1] = new DoubleButton(i+":"+i);
				doubleButtons[i-1].y = btnsCount * 55;
				doubleButtons[i-1].x = (i-1)* 105+205+(Std.int(i/4)*5);
				addChild(doubleButtons[i-1]);
				GameModel.getInstance().initGameType(4);
			}
		}
		
		cast(getChildAt(0), BaseButton).switchState("down");
		
		numberField = new NumberField();
		numberField.x = 210;
		//numberField.y = this.height / 2;
		addChild(numberField);

		numberField.updateRange();
		
		upButton = new UpButton();
		upButton.x = numberField.x + 15;
		upButton.y = numberField.y + 35;
		addChild(upButton);
		
		downButton = new DownButton();
		downButton.x = numberField.x + 15;
		downButton.y = numberField.y + 85;
		addChild(downButton);
		
		rollButton = new RollDiceButton();
		rollButton.x = 845;
		rollButton.y = 165;
		addChild(rollButton);
		
		betField = new BetField();
		betField.x = doubleButtons[3].x+5;
		addChild(betField);
	}
	public function deselectAllButtons():Void {
		for (i in 0...numChildren) {
			if(Std.is(getChildAt(i), BaseButton)) cast(getChildAt(i), BaseButton).switchState("up");
		}
	}
	public function switchNumberField():Void {
		numberField.updateRange();
	}
	public function updateNumberField(operation:EOperation):Void {
		switch(operation) {
			case EOperation.NUMBERFIELD_DOWN: 	numberField.decrease();
			case EOperation.NUMBERFIELD_UP: 	numberField.increase();
			default:
		}
		if (GameModel.getInstance().gameType == GameModel.DOUBLE) {
			deselectAllButtons();
			doubleButtons[NumberFieldModel.getInstance().number-1].switchState("down");
		}
	}
	public function updateBetField(operation:EOperation):Void {
		betField.changeBet(operation);
	}
	public function setDouble(doubleType:Int):Void {
		numberField.updateRangeForDouble(doubleType);
	}
	
}