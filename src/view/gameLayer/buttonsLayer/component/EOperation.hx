package view.gameLayer.buttonsLayer.component;

/**
 * @author Igor Skotnikov
 */
enum EOperation
{
	NUMBERFIELD_DOWN;
	NUMBERFIELD_UP;
	BET_DOWN;
	BET_UP;
}