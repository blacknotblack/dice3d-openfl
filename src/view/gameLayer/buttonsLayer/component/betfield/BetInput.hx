package view.gameLayer.buttonsLayer.component.betfield;
import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFieldType;
import openfl.events.KeyboardEvent;
import data.multiplayer.Room;
import view.gameLayer.buttonsLayer.component.EOperation;

/**
 * ...
 * @author Igor Skotnikov
 */
class BetInput extends TextField
{

	public function new()
	{
		super();
		defaultTextFormat = new TextFormat(GameAssets.getInstance().fontRobotoRegular.fontName, 30, 0xffffff, null, null, null, null, null, "right");
		selectable = true;
		embedFonts = true;
		type = TextFieldType.INPUT;
		borderColor = 0xffffff;
		//border = true;
		selectionColor = "#aaaaaa";
		width = 120;
		height = 40;
		text = ""+Room.getInstance().minBet;
		addEventListener(KeyboardEvent.KEY_DOWN, KEY_DOWN_HANDLER);
	}
	
	public function changeBet(operation:EOperation):Void {
		var bet:Float = Room.getInstance().bet;
		switch(operation) {
			case EOperation.BET_DOWN:
				bet--;
				if (bet < Room.getInstance().minBet) bet = Room.getInstance().minBet;
			case EOperation.BET_UP: 	
				bet++;
				if (bet > Room.getInstance().maxBet) bet = Room.getInstance().maxBet;
			default:
		}
		text = "" + bet;
		Room.getInstance().setBet(bet);
	}
	
	function KEY_DOWN_HANDLER(e:KeyboardEvent):Void
	{
		//if ((e.charCode<48 || e.charCode>57) &&  e.charCode!=46) e.preventDefault();
	}
	
	override function __stopTextInput():Void
	{
		super.__stopTextInput();
		if (text == "") text = ""+Room.getInstance().minBet;
		else Room.getInstance().setBet(Std.parseFloat (text));
	}

}