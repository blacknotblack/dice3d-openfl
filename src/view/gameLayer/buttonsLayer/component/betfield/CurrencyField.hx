package view.gameLayer.buttonsLayer.component.betfield;
import openfl.text.TextField;
import openfl.text.TextFormat;
import data.multiplayer.Room;

/**
 * ...
 * @author Igor Skotnikov
 */
class CurrencyField extends TextField
{

	public function new()
	{
		super();
		defaultTextFormat = new TextFormat(GameAssets.getInstance().fontRobotoRegular.fontName, 22, 0xffffff, null, null, null, null, null, "right");
		embedFonts = true;
		borderColor = 0xffffff;
		//border = true;
		width = 50;
		height = 32;
		text = AppData.getInstance().currency.toUpperCase();
	}
}