package view.gameLayer.buttonsLayer.component.betfield;
import data.multiplayer.GameModel;
import data.multiplayer.NumberFieldModel;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import openfl.text.TextField;
import openfl.text.TextFormat;
import view.gameLayer.buttonsLayer.component.numberfield.DownButton;
import view.gameLayer.CoefField;
import view.gameLayer.buttonsLayer.component.EOperation;

/**
 * ...
 * @author Igor Skotnikov
 */
class BetField extends Sprite
{

	private var image:Bitmap;
	private var input:BetInput;
	private var upButton:UpButton;
	private var downButton:DownButton;
	private var coefField:CoefField;
	private var currencyField:CurrencyField;
	
	public function new() 
	{
		super();
		image = new Bitmap(GameAssets.getInstance().BET_FIELD, null, true);
		//image.x = image.y = -image.width / 2;
		addChild(image);
		
		input = new BetInput();
		input.x = 110;
		input.y = (image.height - input.height) / 2;
		addChild(input);
		
		upButton = new UpButton();
		upButton.x = 15;
		upButton.y = 35;
		addChild(upButton);
		
		downButton = new DownButton();
		downButton.x = 15;
		downButton.y = 85;
		addChild(downButton);
		
		coefField = new CoefField();
		coefField.x = 200;
		coefField.y = 100;
		addChild(coefField);
		
		currencyField = new CurrencyField();
		currencyField.x = 110+input.width;
		currencyField.y = input.y+input.height-currencyField.height;
		addChild(currencyField);
	}
	
	public function changeBet(operation:EOperation):Void {
		input.changeBet(operation);
	}
	
}