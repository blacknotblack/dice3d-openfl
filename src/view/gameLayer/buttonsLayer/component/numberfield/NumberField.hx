package view.gameLayer.buttonsLayer.component.numberfield;
import data.multiplayer.GameModel;
import data.multiplayer.NumberFieldModel;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import openfl.text.TextField;
import openfl.text.TextFormat;

/**
 * ...
 * @author Igor Skotnikov
 */
class NumberField extends Sprite
{

	private var image:Bitmap;
	private var tf:TextField;
	
	private var range:Array<String>;
	private var index:Int;
	
	public function new() 
	{
		super();
		image = new Bitmap(GameAssets.getInstance().NUMBER_FIELD, null, true);
		//image.x = image.y = -image.width / 2;
		addChild(image);
		
		tf 						= new TextField();
		tf.defaultTextFormat	= new TextFormat(GameAssets.getInstance().fontRobotoRegular.fontName, 60, 0xffffff, null, null, null, null, null, "center");
		tf.embedFonts 			= true;
		tf.selectable 			= false;
		tf.multiline 			= false;
		tf.wordWrap 			= false;
		tf.width				= 150;
		tf.height				= 70;
		//tf.borderColor			= 0xfffffff;
		//tf.border				= true;
		tf.text					= "0";
		tf.x = 100;
		tf.y = this.height/2 - 45;
		addChild(tf);
	}
	
	public function updateRange():Void {
		range = NumberFieldModel.getInstance().getCurrentModel(GameModel.getInstance().gameType);
		if (range.indexOf(tf.text) == -1) {
			index = 0;
			updateField(range[0]);
		}
	}
	
	public function updateField(text:String) {
		//var size:Int = 80;
		if (text == NumberFieldModel.getInstance().getCurrentModel(GameModel.ALLDOUBLES)[0]) {
			tf.defaultTextFormat	= new TextFormat(GameAssets.getInstance().fontRobotoRegular.fontName, 22, 0xffffff, null, null, null, null, null, "center");
			tf.y = this.height/2 - 15;
		}
		else {
			tf.defaultTextFormat	= new TextFormat(GameAssets.getInstance().fontRobotoRegular.fontName, 60, 0xffffff, null, null, null, null, null, "center");
			tf.y = this.height/2 - 45;
		}
		tf.text = text;
		//while (tf.width<tf.textWidth) {
			//tf.defaultTextFormat = new TextFormat(GameAssets.getInstance().font.fontName, size--, 0x000000, null, null, null, null, null, "center");
			//tf.text = text;
		//}
		NumberFieldModel.getInstance().setCurrentNumber(text);
	}
	
	public function increase():Void {
		if (index < range.length-1) index++;
		updateField(range[index]);
	}
	public function decrease():Void {
		if (index > 0) index--;
		updateField(range[index]);
	}
	public function updateRangeForDouble(doubleType:Int):Void {
		index = doubleType - 1;
		updateField(range[index]);
	}
	
}