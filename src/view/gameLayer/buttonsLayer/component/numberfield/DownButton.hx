package view.gameLayer.buttonsLayer.component.numberfield;
import events.GameEvent;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.events.TouchEvent;
import openfl.geom.Matrix;

/**
 * ...
 * @author Igor Skotnikov
 */
class DownButton extends ArrowBaseButton
{
	
	public function new() 
	{
		super(EOperation.NUMBERFIELD_DOWN);
		var m:Matrix = new Matrix();
		m.translate( -arrow.width / 2, -arrow.height / 2);
		m.rotate(Math.PI);
		m.translate(arrow.width / 2, arrow.height / 2);
		arrow.transform.matrix = m;
	}	
}