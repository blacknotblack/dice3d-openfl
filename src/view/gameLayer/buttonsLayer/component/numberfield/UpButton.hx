package view.gameLayer.buttonsLayer.component.numberfield;
import events.GameEvent;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.events.TouchEvent;

/**
 * ...
 * @author Igor Skotnikov
 */
class UpButton extends ArrowBaseButton
{
	
	public function new() 
	{
		super(EOperation.NUMBERFIELD_UP);
	}	
}