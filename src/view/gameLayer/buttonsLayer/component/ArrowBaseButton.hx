package view.gameLayer.buttonsLayer.component;
import events.GameEvent;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.events.TouchEvent;

/**
 * ...
 * @author Igor Skotnikov
 */
class ArrowBaseButton extends Sprite
{
	private var up:Bitmap;
	private var	over:Bitmap;
	private var down:Bitmap;
	private var arrow:Bitmap;
	
	private var operation:EOperation;
	
	public function new(operation:EOperation) 
	{
		super();
		this.operation = operation;
		up 	 = new Bitmap(Assets.getBitmapData("assets/img/control/ArrowUpBtn.png"), null, true);
		down = new Bitmap(Assets.getBitmapData("assets/img/control/ArrowDownBtn.png"), null, true);
		over = new Bitmap(Assets.getBitmapData("assets/img/control/ArrowOverBtn.png"), null, true);
		arrow = new Bitmap(Assets.getBitmapData("assets/img/control/ArrowForBtn.png"), null, true);
		
		addChild(up);
		addChild(arrow);
		if (untyped __js__("ismob()")) {
			addEventListener(TouchEvent.TOUCH_BEGIN, TOUCH_HANDLER);
			addEventListener(TouchEvent.TOUCH_END,   TOUCH_HANDLER);
		}
		else{
			addEventListener(MouseEvent.MOUSE_DOWN, MOUSE_HANDLER);
			addEventListener(MouseEvent.MOUSE_UP, 	MOUSE_HANDLER);
			addEventListener(MouseEvent.MOUSE_OVER, MOUSE_HANDLER);
		}
	}
	
	private function MOUSE_HANDLER(e:MouseEvent):Void {
		removeChildAt(0);
		switch(e.type) {
			case MouseEvent.MOUSE_DOWN:
				addChildAt(down, 0);
				GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.ARROW_BUTTON_PRESSED, {"operation":operation}));
			case MouseEvent.MOUSE_UP:
				addChildAt(up, 0);
			case MouseEvent.MOUSE_OVER:
				addChildAt(over, 0);
		}
	}
	private function TOUCH_HANDLER(e:TouchEvent):Void {
		removeChildAt(0);
		switch(e.type) {
			case TouchEvent.TOUCH_BEGIN:
				addChildAt(down, 0);
				GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.ARROW_BUTTON_PRESSED, {"operation":operation}));
			case TouchEvent.TOUCH_END:
				addChildAt(up, 0);
		}
	}
	
}