package view.gameLayer.buttonsLayer;

import data.multiplayer.GameModel;
import events.GameEvent;
import view.gameLayer.buttonsLayer.gameTypeButton.button.BaseButton;
import view.gameLayer.buttonsLayer.component.EOperation;

/**
 * ...
 * @author Igor Skotnikov
 */
class ButtonsLayerMediator
{
	var view:ButtonsLayer;
	
	public function new(view:ButtonsLayer) 
	{
		this.view = view;
		setupListener();
	}
	
	inline private function setupListener() {
		GlobalEventDispatcher.getInstance().eventDispatcher.addEventListener(GameEvent.GAME_TYPE_BUTTON_SELECTED, GAME_TYPE_BUTTON_SELECTED_HANDLER);
		GlobalEventDispatcher.getInstance().eventDispatcher.addEventListener(GameEvent.ARROW_BUTTON_PRESSED, ARROW_BUTTON_PRESSED_HANDLER);
	}
	
	private function GAME_TYPE_BUTTON_SELECTED_HANDLER(e:GameEvent) {
		GameModel.getInstance().setGameType(e.data.gameType);
		view.deselectAllButtons();
		view.switchNumberField();
		if (e.data.doubleType > 0) view.setDouble(e.data.doubleType);
		GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.UPDATE_MULTIPLICATOR));
	}
	public function ARROW_BUTTON_PRESSED_HANDLER(e:GameEvent) {
		switch(e.data.operation) {
			case EOperation.NUMBERFIELD_DOWN | EOperation.NUMBERFIELD_UP:
				view.updateNumberField(e.data.operation);
				GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.UPDATE_MULTIPLICATOR));
			case EOperation.BET_DOWN | EOperation.BET_UP:
				view.updateBetField(e.data.operation);
		}
	}
}