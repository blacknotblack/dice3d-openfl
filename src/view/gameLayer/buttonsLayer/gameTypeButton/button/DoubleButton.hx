package view.gameLayer.buttonsLayer.gameTypeButton.button;
import openfl.display.Bitmap;
import openfl.Assets;

/**
 * ...
 * @author Igor Skotnikov
 */
class DoubleButton extends BaseButton
{

	public function new(text:String) 
	{
		super(text, "assets/img/control/MidBtnUp.png", "assets/img/control/MidBtnDown.png", "assets/img/control/MidBtnOver.png");
		gameMode = 4;
		doubleType = Std.parseInt(text);
	}
	
}