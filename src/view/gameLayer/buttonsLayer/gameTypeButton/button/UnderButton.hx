package view.gameLayer.buttonsLayer.gameTypeButton.button;

/**
 * ...
 * @author Igor Skotnikov
 */
class UnderButton extends BaseButton
{

	public function new() 
	{
		super(Localization.getInstance().data.underGameTypeButton, "assets/img/control/BigBtnUp.png", "assets/img/control/BigBtnDown.png", "assets/img/control/BigBtnOver.png");
		gameMode = 2;
	}
	
}