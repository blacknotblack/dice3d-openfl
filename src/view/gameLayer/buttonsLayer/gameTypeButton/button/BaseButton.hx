package view.gameLayer.buttonsLayer.gameTypeButton.button;

import openfl.geom.Rectangle;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import openfl.Assets;
import openfl.geom.Point;
import openfl.text.TextField;
import openfl.text.TextFormat;

/**
 * ...
 * @author Igor Skotnikov
 */
class BaseButton extends Sprite
{

	public var isSelected:Bool;
	
	public var gameMode(default, null):Int;
	public var doubleType(default, null):Int=-1;
	
	private var mediator:BaseButtonMediator;
	
	private var up:Bitmap;
	private var	over:Bitmap;
	private var down:Bitmap;
	private var tf:TextField;
	
	public function new(text:String, textSize:Int=18, upURL:String, downURL:String, overURL:String)
	{
		super();
		
		mediator = new BaseButtonMediator(this);
		up = new Bitmap(Assets.getBitmapData(upURL), null, true);
		down = new Bitmap(Assets.getBitmapData(downURL), null, true);
		over = new Bitmap(Assets.getBitmapData(overURL), null, true);
		
		addChild(up);
		
		tf 						= new TextField();
		tf.defaultTextFormat	= new TextFormat(GameAssets.getInstance().fontRobotoRegular.fontName, textSize, 0xffffff, null, null, null, null, null, "center");
		tf.embedFonts 			= true;
		tf.selectable 			= false;
		tf.multiline 			= false;
		tf.wordWrap 			= false;
		tf.width				= up.width - 30;
		tf.height				= 40;
		//tf.border				= true;
		//tf.borderColor 		= 0xffff;
		tf.text					= text;
		while (tf.width < tf.textWidth + 10) {
			tf.defaultTextFormat	= new TextFormat(GameAssets.getInstance().fontRobotoRegular.fontName, textSize--, 0xffffff, null, null, null, null, null, "center");
			tf.text = text;
		}
		var bmd:BitmapData = new BitmapData(Std.int(tf.width), Std.int(tf.height), false, 0xfff);
		bmd.draw(tf);
		var top:Int = topPoint(bmd);
		var bottom:Int = 40-bottomPoint(bmd);
		//tf.height = tf.textHeight;
		tf.y = (up.height - tf.height - top+bottom) / 2+2;
		tf.x = (up.width - tf.width) / 2;
		addChild(tf);
	}
	
	public function switchState(state:String):Void {
		removeChildAt(0);
		switch(state) {
			case "up": 		addChildAt(up, 0); 		isSelected = false; 		/* tf.textColor = 0x0; */
			case "down":	addChildAt(down, 0); 	isSelected = true;			/* tf.textColor = 0xffffff; */
			case "over":	if (isSelected) { addChildAt(down, 0); /* tf.textColor = 0xffffff; */} else { addChildAt(over, 0);  /* tf.textColor = 0x0; */}
			case "out":		if (isSelected) { addChildAt(down, 0); /* tf.textColor = 0xffffff; */} else { addChildAt(up, 0);    /* tf.textColor = 0x0; */}
		}
	}
	
	public function deselect():Void {
		switchState("up");
	}
	
	private function topPoint(bmd:BitmapData):Int {
		for (i in 0...40)
			for (j in 0...100)
				if (bmd.getPixel(j, i) == 0xffffff) return i;
		return 0;
	}
	private function bottomPoint(bmd:BitmapData):Int {
		for (i in 0...bmd.height)
			for (j in 0...100)
				if (bmd.getPixel(j, bmd.height-i) == 0xffffff) return bmd.height-i;
		return bmd.height;
	}
	private function leftPoint(bmd:BitmapData):Int {
		for (i in 0...100)
			for (j in 0...40)
				if (bmd.getPixel(i, j) == 0xffffff) return i;
		return 0;
	}
	private function rightPoint(bmd:BitmapData):Int {
		for (i in 0...99)
			for (j in 0...40)
				if (bmd.getPixel(99-i, j) == 0xffffff) return 99-i;
		return 100;
	}
}