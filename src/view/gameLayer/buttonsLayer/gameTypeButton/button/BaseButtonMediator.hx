package view.gameLayer.buttonsLayer.gameTypeButton.button;

import events.GameEvent;
import openfl.events.MouseEvent;
import openfl.events.TouchEvent;
import view.gameLayer.buttonsLayer.gameTypeButton.button.BaseButton;

/**
 * ...
 * @author Igor Skotnikov
 */
class BaseButtonMediator
{
	var view:BaseButton;
	
	public function new(view:BaseButton) 
	{
		this.view = view;
		init();
	}
	
	inline private function init() {
		if (untyped __js__("ismob()")) {
			view.addEventListener(TouchEvent.TOUCH_BEGIN, TOUCH_HANDLER);
		}
		else{
			view.addEventListener(MouseEvent.MOUSE_DOWN, MOUSE_HANDLER);
			view.addEventListener(MouseEvent.MOUSE_OVER, MOUSE_HANDLER);
			view.addEventListener(MouseEvent.MOUSE_OUT,  MOUSE_HANDLER);
		}
	}
	
	private function MOUSE_HANDLER(e:MouseEvent) {
		switch(e.type) {
			case MouseEvent.MOUSE_DOWN: 
				GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.GAME_TYPE_BUTTON_SELECTED, {gameType:view.gameMode, doubleType:view.doubleType}));
				view.switchState("down");
			case MouseEvent.MOUSE_OVER:
				view.switchState("over");
			case MouseEvent.MOUSE_OUT:
				view.switchState("out");
		}	
	}
	
	private function TOUCH_HANDLER(e:MouseEvent) {
		GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.GAME_TYPE_BUTTON_SELECTED, {gameType:view.gameMode, doubleType:view.doubleType}));
		view.switchState("down");
	}
}