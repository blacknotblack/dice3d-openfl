package view.gameLayer.buttonsLayer.gameTypeButton.button;

/**
 * ...
 * @author Igor Skotnikov
 */
class EqualButton extends BaseButton
{

	public function new() 
	{
		super(Localization.getInstance().data.equalGameTypeButton, "assets/img/control/BigBtnUp.png", "assets/img/control/BigBtnDown.png", "assets/img/control/BigBtnOver.png");
		gameMode = 3;
	}
	
}