package view.gameLayer.buttonsLayer.gameTypeButton.button;

/**
 * ...
 * @author Igor Skotnikov
 */
class AllDoubleButton extends BaseButton
{

	public function new() 
	{
		super(Localization.getInstance().data.allDoubleGameTypeButton, "assets/img/control/BigBtnUp.png", "assets/img/control/BigBtnDown.png", "assets/img/control/BigBtnOver.png");
		gameMode = 5;
	}
	
}