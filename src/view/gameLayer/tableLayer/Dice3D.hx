package view.gameLayer.tableLayer;

import lime.graphics.opengl.GL;
import openfl.geom.Point;
import openfl.geom.Vector3D;
import openfl.utils.Float32Array;
import openfl.utils.Int16Array;
import openfl.Vector;
import openfl.geom.Rectangle;


class Dice3D extends Object3D{
	
	
	/* CONSTRUCTOR */
	public function new(data:ObjLoader) {
		super(data);
	}
	/* API */
	
	override private function initBuffers():Void
	{	
		var vertexPositionData	:Array<Float> = [];
        var normalData			:Array<Float> = [];
        var textureCoordData	:Array<Float> = [];
		var indexData			:Array<Int> = [];
		
		vertexPositionData = objLoader.m_indexedVertices;
		normalData = objLoader.m_indexedNormals;
		textureCoordData = objLoader.m_indexedUVs;
		for (i in 0...Std.int(normalData.length/3)) {
			indexData.push(i);
		}
		
		var tangent:Array<Float> = [];
		var bitangent:Array<Float> = [];
		
		var i = 0;
		while (i < indexData.length/3) {
			var i1:Int = indexData[i*3];
			var i2:Int = indexData[i*3+1];
			var i3:Int = indexData[i*3+2];
			i += 1;
			
			var pos1:Vector3D = new Vector3D(vertexPositionData[i1], vertexPositionData[i1 + 1], vertexPositionData[i1 + 2]);
			var pos2:Vector3D = new Vector3D(vertexPositionData[i2], vertexPositionData[i2 + 1], vertexPositionData[i2 + 2]);
			var pos3:Vector3D = new Vector3D(vertexPositionData[i3], vertexPositionData[i3 + 1], vertexPositionData[i3 + 2]);
			
			var uv1:Point = new Point(textureCoordData[i1], textureCoordData[i1 + 1]);
			var uv2:Point = new Point(textureCoordData[i2], textureCoordData[i2 + 1]);
			var uv3:Point = new Point(textureCoordData[i3], textureCoordData[i3 + 1]);
			
			var edge1:Vector3D = pos2.subtract(pos1);
			var edge2:Vector3D = pos3.subtract(pos1);
			
			var deltaUV1:Point = uv2.subtract(uv1);
			var deltaUV2:Point = uv3.subtract(uv1);
			var d = (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);
			var f;
			if (d == 0) f = 1.0;
			else 		f = 1 / d;
			var tangent1:Vector3D = new Vector3D();			
			tangent1.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
			tangent1.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
			tangent1.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
			tangent1.normalize();
			tangent[i1 * 3] = tangent1.x;
			tangent[i1 * 3+1] = tangent1.y;
			tangent[i1 * 3+2] = tangent1.z;
			tangent[i2 * 3] = tangent1.x;
			tangent[i2 * 3+1] = tangent1.y;
			tangent[i2 * 3+2] = tangent1.z;
			tangent[i3 * 3] = tangent1.x;
			tangent[i3 * 3+1] = tangent1.y;
			tangent[i3 * 3+2] = tangent1.z;
			
			var bitangent1:Vector3D = new Vector3D();
			bitangent1.x = f * (-deltaUV2.x * edge1.x + deltaUV1.x * edge2.x);
			bitangent1.y = f * (-deltaUV2.x * edge1.y + deltaUV1.x * edge2.y);
			bitangent1.z = f * (-deltaUV2.x * edge1.z + deltaUV1.x * edge2.z);
			bitangent1.normalize();
			bitangent[i1 * 3] = bitangent1.x;
			bitangent[i1 * 3+1] = bitangent1.y;
			bitangent[i1 * 3+2] = bitangent1.z;
			bitangent[i2 * 3] = bitangent1.x;
			bitangent[i2 * 3+1] = bitangent1.y;
			bitangent[i2 * 3+2] = bitangent1.z;
			bitangent[i3 * 3] = bitangent1.x;
			bitangent[i3 * 3+1] = bitangent1.y;
			bitangent[i3 * 3+2] = bitangent1.z;
		}
		
		vertexNormalBuffer = GL.createBuffer();
        GL.bindBuffer(GL.ARRAY_BUFFER, vertexNormalBuffer);
        GL.bufferData(GL.ARRAY_BUFFER, 4*normalData.length, new Float32Array(normalData), GL.STATIC_DRAW);
		GL.bindBuffer(GL.ARRAY_BUFFER, null);
        vertexNormalBuffer_ItemSize = 3;
        vertexNormalBuffer_NumItems = Std.int(normalData.length / 3);

        vertexTextureCoordBuffer = GL.createBuffer();
        GL.bindBuffer(GL.ARRAY_BUFFER, vertexTextureCoordBuffer);
        GL.bufferData(GL.ARRAY_BUFFER, 4*textureCoordData.length, new Float32Array(textureCoordData), GL.STATIC_DRAW);
		GL.bindBuffer(GL.ARRAY_BUFFER, null);
        vertexTextureCoordBuffer_ItemSize = 2;
        vertexTextureCoordBuffer_NumItems = Std.int(textureCoordData.length / 2);

        vertexPositionBuffer = GL.createBuffer();
        GL.bindBuffer(GL.ARRAY_BUFFER, vertexPositionBuffer);
        GL.bufferData(GL.ARRAY_BUFFER, 4*vertexPositionData.length, new Float32Array(vertexPositionData), GL.STATIC_DRAW);
		GL.bindBuffer(GL.ARRAY_BUFFER, null);
        vertexPositionBuffer_ItemSize = 3;
        vertexPositionBuffer_NumItems = Std.int(vertexPositionData.length / 3);
		
		tangentBuffer = GL.createBuffer();
        GL.bindBuffer(GL.ARRAY_BUFFER, tangentBuffer);
        GL.bufferData(GL.ARRAY_BUFFER, 4*tangent.length, new Float32Array(tangent), GL.STATIC_DRAW);
		GL.bindBuffer(GL.ARRAY_BUFFER, null);
        tangentBuffer_ItemSize = 3;
        tangentBuffer_NumItems = Std.int(tangent.length / 3);
		
		bitangentBuffer = GL.createBuffer();
        GL.bindBuffer(GL.ARRAY_BUFFER, bitangentBuffer);
        GL.bufferData(GL.ARRAY_BUFFER, 4*bitangent.length, new Float32Array(bitangent), GL.STATIC_DRAW);
		GL.bindBuffer(GL.ARRAY_BUFFER, null);
        bitangentBuffer_ItemSize = 3;
        bitangentBuffer_NumItems = Std.int(bitangent.length / 3);
		
		vertexIndexBuffer = GL.createBuffer();
        GL.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, vertexIndexBuffer);
        GL.bufferData(GL.ELEMENT_ARRAY_BUFFER, 2*indexData.length, new Int16Array(indexData), GL.STATIC_DRAW);
		GL.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, null);
        vertexIndexBuffer_ItemSize = 1;
        vertexIndexBuffer_NumItems = indexData.length;
	}
	
	override public function setupMatrix(rect:Rectangle) {
		lightSpaceMatrix = new JSMatrix();
		lightSpaceMatrix.setPerspective(70, 1, 1, 200);
		lightSpaceMatrix.lookAtFloat(0*superscale, 7*superscale, 1*superscale, 0, 0, 0, 0, 1, 0);
		lightSpaceMatrix.prependRotation( 135, new Vector3D(0, 1, 0));
		projectionMatrix = new JSMatrix();
		projectionMatrix.setPerspective(45, rect.width / rect.height, 0.1, 200);
		modelViewMatrix = new JSMatrix();
		modelViewMatrix.lookAtFloat(-4*superscale*zoom, 4*superscale*zoom, 2.5*superscale*zoom, 0, 0, 0, 0, 1, 0);
	}
	
	public function updatePosition(positionData:Dynamic):Void {
		modelMatrix.rawData = Vector.ofArray ([ 1.7/20*superscale, 0, 0, 0,
												0, 1.7/20*superscale, 0, 0,
												0, 0, 1.7/20*superscale, 0,
												positionData.x/20*superscale, positionData.y/20*superscale, positionData.z/20*superscale, 1 ]);
		modelMatrix.prependRotation(positionData.ex*180/Math.PI, new Vector3D(1, 0, 0));
		modelMatrix.prependRotation(positionData.ey*180/Math.PI, new Vector3D(0, 1, 0));
		modelMatrix.prependRotation(positionData.ez*180/Math.PI, new Vector3D(0, 0, 1));
	}
}