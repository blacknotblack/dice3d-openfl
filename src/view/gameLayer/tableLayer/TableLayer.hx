package view.gameLayer.tableLayer;

import haxe.Json;
import js.Browser;
import lime.graphics.opengl.GL;
import lime.graphics.opengl.GLFramebuffer;
import lime.graphics.opengl.GLProgram;
import lime.graphics.opengl.GLShader;
import lime.graphics.opengl.GLTexture;
import lime.graphics.opengl.GLUniformLocation;
import openfl.Assets;
import openfl.display.FPS;
import openfl.display.OpenGLView;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.geom.Rectangle;
import openfl.gl.GLRenderbuffer;

/**
 * ...
 * @author Igor Skotnikov
 */
class TableLayer extends Sprite 
{
	
	public static var BALL_RADIUS:Int = 9;
	
	private var table:Table3D;
	private var dice1:Dice3D;
	private var dice2:Dice3D;
	private var obj:Dynamic;
	private var objLoaderTable:ObjLoader;
	private var objLoaderDice:ObjLoader;
	
	private var GLView:OpenGLView;
	private var shaderDepthProgram:GLProgram;
	private var shaderProgram:GLProgram;
	private var vertexAttrib:Int;
	private var vertexNormalAttribute:Int;
	private var textureCoordAttribute:Int;
	private var tangentAttribute:Int;
	private var bitangentAttribute:Int;
	
	private var projectionMatrixUniform:GLUniformLocation;
	private var modelViewMatrixUniform:GLUniformLocation;
	private var modelMatrixUniform:GLUniformLocation;
	private var samplerUniform:GLUniformLocation;
	private var samplerNormalUniform:GLUniformLocation;
	private var lightSpaceMatrixUniform:GLUniformLocation;
	private var samplerShadowUniform:GLUniformLocation;
	
	private var vertexDepthAttrib:Int;
	private var lightSpaceDepthMatrixUniform:GLUniformLocation;
	private var modelDepthMatrixUniform:GLUniformLocation;

	private var str:String = '{"dice1":[{"x":-1,"y":15,"z":-25,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":14.962,"z":-23.013,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":14.906,"z":-21.689,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":14.718,"z":-19.041,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":14.512,"z":-17.054,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":14.343,"z":-15.73,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":13.931,"z":-13.082,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":13.418,"z":-10.433,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":12.968,"z":-8.447,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":12.637,"z":-7.123,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":11.281,"z":-2.488,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":10.368,"z":0.16,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":9.356,"z":2.808,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":8.243,"z":5.457,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":7.343,"z":7.443,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":6.712,"z":8.767,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":6.056,"z":10.091,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":4.668,"z":12.74,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":3.937,"z":14.064,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":2.793,"z":16.05,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-1,"y":2,"z":17.374,"ex":1.047,"ey":0.523,"ez":0.785},{"x":-0.857,"y":1.486,"z":19.726,"ex":2.098,"ey":0.393,"ez":0.477},{"x":-0.76,"y":1.899,"z":20.653,"ex":2.808,"ey":0.045,"ez":0.152},{"x":-0.615,"y":2.472,"z":22.043,"ex":-2.471,"ey":-0.638,"ez":0.229},{"x":-0.518,"y":2.823,"z":22.97,"ex":-1.433,"ey":-0.846,"ez":0.808},{"x":-0.324,"y":3.45,"z":24.823,"ex":0.223,"ey":-0.121,"ez":1.416},{"x":-0.227,"y":3.726,"z":25.75,"ex":0.88,"ey":0.293,"ez":1.195},{"x":-0.033,"y":4.203,"z":27.604,"ex":2.488,"ey":0.241,"ez":0.273},{"x":0.015,"y":4.307,"z":28.067,"ex":2.82,"ey":0.037,"ez":0.149},{"x":0.208,"y":4.659,"z":29.921,"ex":-1.98,"ey":-0.801,"ez":0.477},{"x":0.305,"y":4.798,"z":30.848,"ex":-0.867,"ey":-0.757,"ez":1.14},{"x":0.397,"y":5.105,"z":30.58,"ex":-0.237,"ey":-1.247,"ez":0.595},{"x":0.421,"y":5.168,"z":30.489,"ex":-0.121,"ey":-1.398,"ez":0.464},{"x":0.516,"y":5.357,"z":30.128,"ex":-2.747,"ey":-1.136,"ez":3.135},{"x":0.564,"y":5.413,"z":29.948,"ex":-2.485,"ey":-0.864,"ez":2.866},{"x":0.659,"y":5.452,"z":29.587,"ex":-1.879,"ey":-0.495,"ez":2.256},{"x":0.706,"y":5.434,"z":29.406,"ex":-1.542,"ey":-0.442,"ez":1.918},{"x":0.777,"y":5.36,"z":29.135,"ex":-1.043,"ey":-0.554,"ez":1.416},{"x":0.825,"y":5.279,"z":28.955,"ex":-0.739,"ey":-0.741,"ez":1.11},{"x":0.92,"y":5.042,"z":28.594,"ex":-0.213,"ey":-1.277,"ez":0.569},{"x":0.967,"y":4.887,"z":28.413,"ex":-2.865,"ey":-1.558,"ez":-2.572},{"x":1.039,"y":4.606,"z":28.142,"ex":-2.722,"ey":-1.107,"ez":3.108},{"x":1.086,"y":4.388,"z":27.962,"ex":-2.457,"ey":-0.839,"ez":2.838},{"x":1.181,"y":3.876,"z":27.601,"ex":-1.846,"ey":-0.485,"ez":2.222},{"x":1.229,"y":3.583,"z":27.42,"ex":-1.508,"ey":-0.442,"ez":1.883},{"x":1.324,"y":2.922,"z":27.059,"ex":-0.857,"ey":-0.657,"ez":1.229},{"x":1.348,"y":2.741,"z":26.969,"ex":-0.711,"ey":-0.763,"ez":1.081},{"x":1.443,"y":1.954,"z":26.608,"ex":-0.189,"ey":-1.307,"ez":0.543},{"x":1.49,"y":1.524,"z":26.427,"ex":-3.008,"ey":-1.527,"ez":-2.765},{"x":1.506,"y":1.151,"z":26.138,"ex":-2.958,"ey":-1.232,"ez":-3.1},{"x":1.479,"y":1.209,"z":26.034,"ex":-3.134,"ey":-1.224,"ez":3.118},{"x":1.447,"y":1.312,"z":25.916,"ex":3.098,"ey":-1.218,"ez":-3.115},{"x":1.438,"y":1.368,"z":25.855,"ex":3.067,"ey":-1.207,"ez":-3.086},{"x":1.431,"y":1.44,"z":25.757,"ex":3.059,"ey":-1.186,"ez":-2.997},{"x":1.435,"y":1.459,"z":25.719,"ex":3.081,"ey":-1.175,"ez":-2.937},{"x":1.451,"y":1.467,"z":25.678,"ex":3.141,"ey":-1.156,"ez":-2.834},{"x":1.46,"y":1.47,"z":25.663,"ex":-3.12,"ey":-1.145,"ez":-2.791},{"x":1.458,"y":1.482,"z":25.648,"ex":-3.133,"ey":-1.146,"ez":-2.791},{"x":1.454,"y":1.481,"z":25.65,"ex":-3.136,"ey":-1.146,"ez":-2.798},{"x":1.443,"y":1.473,"z":25.667,"ex":-3.138,"ey":-1.146,"ez":-2.819},{"x":1.433,"y":1.462,"z":25.687,"ex":-3.139,"ey":-1.146,"ez":-2.84},{"x":1.406,"y":1.424,"z":25.747,"ex":-3.141,"ey":-1.145,"ez":-2.906},{"x":1.386,"y":1.395,"z":25.786,"ex":-3.141,"ey":-1.145,"ez":-2.951},{"x":1.342,"y":1.311,"z":25.885,"ex":-3.141,"ey":-1.145,"ez":-3.066},{"x":1.328,"y":1.283,"z":25.913,"ex":3.141,"ey":-1.145,"ez":-3.101},{"x":1.299,"y":1.242,"z":25.975,"ex":-3.141,"ey":-1.144,"ez":3.101},{"x":1.301,"y":1.253,"z":25.973,"ex":-3.141,"ey":-1.143,"ez":3.106},{"x":1.313,"y":1.236,"z":25.954,"ex":-3.141,"ey":-1.141,"ez":-3.137},{"x":1.315,"y":1.241,"z":25.951,"ex":-3.141,"ey":-1.136,"ez":-3.138},{"x":1.314,"y":1.245,"z":25.952,"ex":-3.141,"ey":-1.136,"ez":-3.14},{"x":1.314,"y":1.247,"z":25.952,"ex":-3.141,"ey":-1.136,"ez":-3.14},{"x":1.314,"y":1.248,"z":25.953,"ex":-3.141,"ey":-1.136,"ez":-3.141},{"x":1.314,"y":1.249,"z":25.953,"ex":-3.141,"ey":-1.136,"ez":-3.141},{"x":1.314,"y":1.249,"z":25.953,"ex":3.141,"ey":-1.136,"ez":-3.141},{"x":1.314,"y":1.249,"z":25.953,"ex":3.141,"ey":-1.136,"ez":-3.141},{"x":1.314,"y":1.249,"z":25.953,"ex":3.141,"ey":-1.136,"ez":-3.141},{"x":1.314,"y":1.249,"z":25.953,"ex":3.141,"ey":-1.136,"ez":-3.141},{"x":1.314,"y":1.249,"z":25.953,"ex":3.141,"ey":-1.136,"ez":3.141},{"x":1.314,"y":1.249,"z":25.953,"ex":3.141,"ey":-1.136,"ez":3.141},{"x":1.314,"y":1.249,"z":25.953,"ex":3.141,"ey":-1.136,"ez":3.141},{"x":1.314,"y":1.249,"z":25.953,"ex":3.141,"ey":-1.136,"ez":3.141},{"x":1.314,"y":1.249,"z":25.953,"ex":3.141,"ey":-1.136,"ez":3.141},{"x":1.314,"y":1.249,"z":25.953,"ex":3.141,"ey":-1.136,"ez":3.141},{"x":1.314,"y":1.249,"z":25.953,"ex":3.141,"ey":-1.136,"ez":3.141},{"x":1.314,"y":1.249,"z":25.953,"ex":3.141,"ey":-1.136,"ez":3.141},{"x":1.314,"y":1.249,"z":25.953,"ex":3.141,"ey":-1.136,"ez":3.141},{"x":1.314,"y":1.249,"z":25.953,"ex":3.141,"ey":-1.136,"ez":3.141}],"dice2":[{"x":3,"y":15,"z":-25,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":14.962,"z":-23.082,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":14.906,"z":-21.804,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":14.718,"z":-19.248,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":14.512,"z":-17.331,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":14.343,"z":-16.053,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":13.931,"z":-13.497,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":13.418,"z":-10.941,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":12.968,"z":-9.024,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":12.637,"z":-7.746,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":11.281,"z":-3.273,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":10.368,"z":-0.717,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":9.356,"z":1.838,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":8.243,"z":4.394,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":7.343,"z":6.312,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":6.712,"z":7.59,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":6.056,"z":8.868,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":4.668,"z":11.424,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":3.937,"z":12.702,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":2.793,"z":14.619,"ex":1.047,"ey":0.785,"ez":0.785},{"x":3,"y":2,"z":15.897,"ex":1.047,"ey":0.785,"ez":0.785},{"x":2.999,"y":1.827,"z":17.378,"ex":1.531,"ey":-0.104,"ez":1.109},{"x":2.999,"y":1.951,"z":17.94,"ex":2.078,"ey":-0.524,"ez":1.607},{"x":2.999,"y":2.091,"z":18.782,"ex":-3.092,"ey":-0.401,"ez":2.655},{"x":2.999,"y":2.153,"z":19.343,"ex":-2.644,"ey":0.096,"ez":3.048},{"x":2.999,"y":2.202,"z":20.466,"ex":-1.84,"ey":1.327,"ez":3.119},{"x":2.999,"y":2.189,"z":21.028,"ex":0.838,"ey":1.132,"ez":0.787},{"x":2.999,"y":2.088,"z":22.15,"ex":1.511,"ey":-0.077,"ez":1.092},{"x":2.999,"y":2.047,"z":22.431,"ex":1.74,"ey":-0.324,"ez":1.294},{"x":2.999,"y":1.821,"z":23.554,"ex":-3.122,"ey":-0.42,"ez":2.628},{"x":2.999,"y":1.671,"z":24.116,"ex":-2.661,"ey":0.068,"ez":3.034},{"x":3.153,"y":1.739,"z":25.194,"ex":-1.96,"ey":0.525,"ez":2.74},{"x":3.204,"y":1.777,"z":25.46,"ex":-1.762,"ey":0.53,"ez":2.582},{"x":3.408,"y":1.868,"z":26.524,"ex":-1.053,"ey":0.305,"ez":2.04},{"x":3.51,"y":1.876,"z":27.055,"ex":-0.782,"ey":0.09,"ez":1.869},{"x":3.715,"y":1.817,"z":28.119,"ex":-0.326,"ey":-0.428,"ez":1.708},{"x":3.817,"y":1.75,"z":28.651,"ex":-0.081,"ey":-0.7,"ez":1.724},{"x":3.97,"y":1.603,"z":29.449,"ex":0.504,"ey":-1.068,"ez":2.012},{"x":4.103,"y":1.635,"z":29.865,"ex":1.27,"ey":-1.213,"ez":2.609},{"x":4.429,"y":1.947,"z":30.468,"ex":2.676,"ey":-0.904,"ez":-2.579},{"x":4.619,"y":2.085,"z":30.651,"ex":2.854,"ey":-0.643,"ez":-2.442},{"x":4.945,"y":2.273,"z":30.747,"ex":2.87,"ey":-0.272,"ez":-2.274},{"x":5.162,"y":2.368,"z":30.811,"ex":2.846,"ey":-0.026,"ez":-2.171},{"x":5.579,"y":2.496,"z":30.816,"ex":2.708,"ey":0.274,"ez":-2.034},{"x":5.779,"y":2.529,"z":30.756,"ex":2.618,"ey":0.329,"ez":-2.003},{"x":6.18,"y":2.521,"z":30.637,"ex":2.427,"ey":0.431,"ez":-1.923},{"x":6.28,"y":2.503,"z":30.607,"ex":2.376,"ey":0.454,"ez":-1.899},{"x":6.681,"y":2.37,"z":30.489,"ex":2.161,"ey":0.534,"ez":-1.789},{"x":6.881,"y":2.265,"z":30.429,"ex":2.047,"ey":0.565,"ez":-1.725},{"x":7.281,"y":1.982,"z":30.31,"ex":1.806,"ey":0.608,"ez":-1.582},{"x":7.482,"y":1.803,"z":30.251,"ex":1.681,"ey":0.618,"ez":-1.506},{"x":7.771,"y":1.524,"z":30.16,"ex":1.432,"ey":0.566,"ez":-1.374},{"x":7.945,"y":1.516,"z":30.128,"ex":1.419,"ey":0.408,"ez":-1.301},{"x":8.283,"y":1.428,"z":30.074,"ex":1.42,"ey":0.071,"ez":-1.198},{"x":8.438,"y":1.352,"z":30.059,"ex":1.419,"ey":-0.127,"ez":-1.192},{"x":8.532,"y":1.447,"z":30.091,"ex":1.478,"ey":-0.203,"ez":-1.124},{"x":8.58,"y":1.494,"z":30.11,"ex":1.533,"ey":-0.254,"ez":-1.089},{"x":8.66,"y":1.545,"z":30.156,"ex":1.656,"ey":-0.329,"ez":-1.009},{"x":8.69,"y":1.552,"z":30.183,"ex":1.724,"ey":-0.353,"ez":-0.965},{"x":8.719,"y":1.545,"z":30.231,"ex":1.835,"ey":-0.373,"ez":-0.901},{"x":8.728,"y":1.544,"z":30.269,"ex":1.88,"ey":-0.384,"ez":-0.872},{"x":8.758,"y":1.558,"z":30.31,"ex":1.897,"ey":-0.413,"ez":-0.873},{"x":8.772,"y":1.562,"z":30.32,"ex":1.904,"ey":-0.422,"ez":-0.871},{"x":8.788,"y":1.566,"z":30.332,"ex":1.911,"ey":-0.43,"ez":-0.868},{"x":8.79,"y":1.566,"z":30.333,"ex":1.91,"ey":-0.43,"ez":-0.868},{"x":8.787,"y":1.564,"z":30.333,"ex":1.905,"ey":-0.424,"ez":-0.87},{"x":8.781,"y":1.561,"z":30.329,"ex":1.898,"ey":-0.417,"ez":-0.872},{"x":8.755,"y":1.551,"z":30.313,"ex":1.876,"ey":-0.393,"ez":-0.881},{"x":8.736,"y":1.543,"z":30.301,"ex":1.861,"ey":-0.376,"ez":-0.886},{"x":8.7,"y":1.527,"z":30.276,"ex":1.834,"ey":-0.344,"ez":-0.896},{"x":8.67,"y":1.512,"z":30.255,"ex":1.812,"ey":-0.317,"ez":-0.903},{"x":8.597,"y":1.468,"z":30.202,"ex":1.755,"ey":-0.247,"ez":-0.918},{"x":8.553,"y":1.437,"z":30.169,"ex":1.722,"ey":-0.205,"ez":-0.926},{"x":8.478,"y":1.373,"z":30.114,"ex":1.664,"ey":-0.127,"ez":-0.934},{"x":8.424,"y":1.318,"z":30.07,"ex":1.619,"ey":-0.068,"ez":-0.935},{"x":8.339,"y":1.258,"z":30.018,"ex":1.569,"ey":0.007,"ez":-0.934},{"x":8.332,"y":1.258,"z":30.01,"ex":1.565,"ey":0.007,"ez":-0.935},{"x":8.336,"y":1.25,"z":30.012,"ex":1.57,"ey":-0.001,"ez":-0.935},{"x":8.336,"y":1.249,"z":30.013,"ex":1.57,"ey":-0.001,"ez":-0.936},{"x":8.335,"y":1.249,"z":30.013,"ex":1.57,"ey":0,"ez":-0.936},{"x":8.335,"y":1.249,"z":30.013,"ex":1.57,"ey":0,"ez":-0.936},{"x":8.335,"y":1.249,"z":30.013,"ex":1.57,"ey":0,"ez":-0.936},{"x":8.335,"y":1.249,"z":30.013,"ex":1.57,"ey":0,"ez":-0.936},{"x":8.335,"y":1.249,"z":30.013,"ex":1.57,"ey":0,"ez":-0.936},{"x":8.335,"y":1.249,"z":30.013,"ex":1.57,"ey":0,"ez":-0.936},{"x":8.335,"y":1.249,"z":30.013,"ex":1.57,"ey":0,"ez":-0.936},{"x":8.335,"y":1.249,"z":30.013,"ex":1.57,"ey":0,"ez":-0.936}]}';
	private var jsn:Dynamic;
	
	private var tableLayerMediator:TableLayerMediator;
	public function new() 
	{
		super();
		objLoaderTable = GameAssets.getInstance().objLoaderTable;
		objLoaderDice = GameAssets.getInstance().objLoaderDice;
		initObjects();
		
		addChild(new FPS(10, 240, 0xffffff));
		
		jsn = Json.parse(str);
		
		tableLayerMediator = new TableLayerMediator(this);
	}
	
	public function parseTraectory(traectory:String):Void {
		jsn = traectory;
		if (!Reflect.hasField(jsn, "dice1") || !Reflect.hasField(jsn, "dice2")) jsn = null;
		clearStepTimer();
	}

	private function initObjects() {
		
		if (OpenGLView.isSupported) {
			GLView = new OpenGLView();
			initShaders();
			initShadowTexture();
			GLView.render = drawScene;
			addChild(GLView);
			
		} else {
			///pewpewpew
		}	
		
		table = new Table3D(objLoaderTable);
		table.setupOpenGL(shaderProgram, shaderDepthProgram, "assets/shader/Table1_Diffuse.png", "assets/shader/Table1_Normal.png");
		addChild(table);
		dice1 = new Dice3D(objLoaderDice);
		dice1.setupOpenGL(shaderProgram, shaderDepthProgram, "assets/shader/diffuse_map.png", "assets/shader/normal_map.png");
		dice2 = new Dice3D(objLoaderDice);
		dice2.setupOpenGL(shaderProgram, shaderDepthProgram, "assets/shader/diffuse_map.png", "assets/shader/normal_map.png");
		
		clearStepTimer();
	}
	
	
	private function initShaders():Void
	{		
		var vertexShader:GLShader = GL.createShader(GL.VERTEX_SHADER);
		GL.shaderSource(vertexShader, Assets.getText("assets/shader/shader_shadow.vert"));
		GL.compileShader(vertexShader);
		if (GL.getShaderParameter(vertexShader, GL.COMPILE_STATUS) == 0) {
			throw "Error, can't compile vertex shader1";
		}
		var fragmentShader:GLShader = GL.createShader(GL.FRAGMENT_SHADER);
		GL.shaderSource(fragmentShader, Assets.getText("assets/shader/shader_shadow.frag"));
		GL.compileShader(fragmentShader);
		if (GL.getShaderParameter(fragmentShader, GL.COMPILE_STATUS) == 0) {
			throw "Error, can't compile fragment shader";
		}		
		shaderProgram = GL.createProgram();
		GL.attachShader(shaderProgram, vertexShader);
		GL.attachShader(shaderProgram, fragmentShader);
		GL.linkProgram(shaderProgram);
		if (GL.getProgramParameter(shaderProgram, GL.LINK_STATUS) == 0) {
			throw "Can't init shader prog.";
		}
		
		var vertexShader2:GLShader = GL.createShader(GL.VERTEX_SHADER);
		GL.shaderSource(vertexShader2, Assets.getText("assets/shader/shader_shadow_depth.vert"));
		GL.compileShader(vertexShader2);
		if (GL.getShaderParameter(vertexShader2, GL.COMPILE_STATUS) == 0) {
			throw "Error, can't compile vertex shader";
		}
		var fragmentShader2:GLShader = GL.createShader(GL.FRAGMENT_SHADER);
		GL.shaderSource(fragmentShader2, Assets.getText("assets/shader/shader_shadow_depth.frag"));
		GL.compileShader(fragmentShader2);
		if (GL.getShaderParameter(fragmentShader2, GL.COMPILE_STATUS) == 0) {
			throw "Error, can't compile fragment shader";
		}		
		shaderDepthProgram = GL.createProgram();
		GL.attachShader(shaderDepthProgram, vertexShader2);
		GL.attachShader(shaderDepthProgram, fragmentShader2);
		GL.linkProgram(shaderDepthProgram);
		if (GL.getProgramParameter(shaderDepthProgram, GL.LINK_STATUS) == 0) {
			throw "Can't init shader prog.";
		}
		
		
		vertexAttrib = GL.getAttribLocation(shaderProgram, "aVertexPosition");
		vertexNormalAttribute = GL.getAttribLocation(shaderProgram, "aVertexNormal");
		textureCoordAttribute  = GL.getAttribLocation(shaderProgram, "aTextureCoord");
		tangentAttribute  = GL.getAttribLocation(shaderProgram, "aTangent");
		bitangentAttribute  = GL.getAttribLocation(shaderProgram, "aBitangent");
		
		projectionMatrixUniform = GL.getUniformLocation (shaderProgram, "uPMatrix");
		modelViewMatrixUniform = GL.getUniformLocation (shaderProgram, "uViewMatrix");
		modelMatrixUniform = GL.getUniformLocation (shaderProgram, "uModelMatrix");
		samplerUniform = GL.getUniformLocation(shaderProgram, "uSampler");
		samplerNormalUniform = GL.getUniformLocation(shaderProgram, "uNormalSampler");
		samplerShadowUniform = GL.getUniformLocation(shaderProgram, "uShadowSampler");
		
		lightSpaceMatrixUniform = GL.getUniformLocation(shaderProgram, "uLightSpaceMatrix");
		
		//depth shader
		vertexDepthAttrib  = GL.getAttribLocation(shaderDepthProgram, "aPos");
		lightSpaceDepthMatrixUniform = GL.getUniformLocation(shaderDepthProgram, "lightSpaceMatrix");
		modelDepthMatrixUniform = GL.getUniformLocation(shaderDepthProgram, "model");
	}
	
	var shadowTexture:GLTexture;
	var shadowBuffer:GLFramebuffer;
	private function initShadowTexture():Void {
		shadowTexture = GL.createTexture();
		GL.bindTexture(GL.TEXTURE_2D, shadowTexture);
		GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, 1024, 1024, 0, GL.RGBA, GL.UNSIGNED_BYTE, null);
		GL.texParameteri (GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR);
		
		shadowBuffer = GL.createFramebuffer();
		
		var depthBuffer:GLRenderbuffer = GL.createRenderbuffer();
		GL.bindRenderbuffer(GL.RENDERBUFFER, depthBuffer);
		GL.renderbufferStorage(GL.RENDERBUFFER, GL.DEPTH_COMPONENT16, 1024, 1024);
		
		GL.bindFramebuffer(GL.FRAMEBUFFER, shadowBuffer);
		GL.framebufferTexture2D(GL.FRAMEBUFFER, GL.COLOR_ATTACHMENT0, GL.TEXTURE_2D, shadowTexture, 0);
		GL.framebufferRenderbuffer(GL.FRAMEBUFFER, GL.DEPTH_ATTACHMENT, GL.RENDERBUFFER, depthBuffer);
		
		GL.bindFramebuffer(GL.FRAMEBUFFER, null);
		GL.bindTexture (GL.TEXTURE_2D, null);
		GL.bindRenderbuffer(GL.RENDERBUFFER, null);
	}
	
	private function drawScene(rect:Rectangle):Void
	{
		if(jsn!=null){
			calculateNextIteration();
			if (_nextIteration >= jsn.dice1.length) _nextIteration = Std.int(jsn.dice1.length - 1);
			dice1.updatePosition(jsn.dice1[_nextIteration]);
			dice2.updatePosition(jsn.dice2[_nextIteration]);
		}
		
		GL.bindFramebuffer(GL.FRAMEBUFFER, shadowBuffer);
		GL.viewport (0, 0, 1024, 1024);
		GL.clear(GL.DEPTH_BUFFER_BIT);
		GL.enable(GL.DEPTH_TEST);
		
		table.setupMatrix(rect);
		if(jsn!=null){
			dice1.setupMatrix(rect);
			dice2.setupMatrix(rect);
		}
		GL.enableVertexAttribArray(vertexDepthAttrib);
		
		GL.useProgram (shaderDepthProgram);
		table.render(true);
		if(jsn!=null){
			dice1.render(true);
			dice2.render(true);
		}
		
		GL.bindFramebuffer(GL.FRAMEBUFFER, null);
		GL.disableVertexAttribArray(vertexDepthAttrib);
		
		GL.viewport (Std.int (_rect.width), Std.int (_rect.height), Std.int (rect.width*0.6), Std.int (rect.height*0.6));
		//GL.viewport (Std.int (rect.x), Std.int (rect.y), Std.int (rect.width), Std.int (rect.height));
		GL.clear(GL.DEPTH_BUFFER_BIT);
		
		//graphics.clear();
		//graphics.lineStyle(5);
		//graphics.drawRect(_rect.width, 0.4*rect.height-_rect.height, rect.width*0.6, rect.height*0.6);

		GL.useProgram(shaderProgram);
		GL.enableVertexAttribArray(vertexAttrib);
		GL.enableVertexAttribArray(textureCoordAttribute);
		GL.enableVertexAttribArray(vertexNormalAttribute);
		GL.enableVertexAttribArray(tangentAttribute);
		GL.enableVertexAttribArray(bitangentAttribute);
		
		table.activeTexture();
		GL.activeTexture (GL.TEXTURE2);
		GL.bindTexture (GL.TEXTURE_2D, shadowTexture);
		GL.uniform1i(samplerShadowUniform, 2);
		table.render(false);
		
		dice1.activeTexture();
		if(jsn!=null){
			dice1.render(false);
			dice2.render(false);
		}
		GL.bindBuffer (GL.ARRAY_BUFFER, null);
		GL.disable(GL.DEPTH_TEST);
		GL.disableVertexAttribArray(vertexAttrib);
		GL.disableVertexAttribArray(textureCoordAttribute);
		GL.disableVertexAttribArray(vertexNormalAttribute);
		GL.disableVertexAttribArray(tangentAttribute);
		GL.disableVertexAttribArray(bitangentAttribute);

		GL.viewport (Std.int (rect.x), Std.int (rect.y), Std.int (rect.width), Std.int (rect.height));
		//GL.clear(GL.DEPTH_BUFFER_BIT);
		GL.useProgram (null);
	}
	
	var _rect:Rectangle = new Rectangle();
	public function resize(width:Float, height:Float, bottomEdge:Float):Void {
		_rect.width = width*0.4;
		if (_rect.width < 400) {
			_rect.width = 400;
			table.resize(0.78);
			dice1.resize(0.78);
			dice2.resize(0.78);
		}
		else {
			table.resize(0.7);
			dice1.resize(0.7);
			dice2.resize(0.7);
		}
		_rect.height = height - bottomEdge;
		//var factor = width / height;
		//table.resize(factor);
	}
	
	var _currentIteration:Int = 0;
	var _nextIteration:Int = 0;
	var	_curTimerGame:Float = 0.0;
	var _prevTimerGame:Float = 0.0;
	private function calculateNextIteration():Void{
		_curTimerGame = Date.now().getTime();
		var elapsedTime = _curTimerGame - _prevTimerGame;
		var elapsedIterations = Math.round(elapsedTime/17);
		_nextIteration += elapsedIterations;
		_prevTimerGame = _curTimerGame;
	}
	private function clearStepTimer():Void {
		_currentIteration = 0;
		_nextIteration = 0;
		_curTimerGame = 0;
		_prevTimerGame = Date.now().getTime();
	}
	
}
