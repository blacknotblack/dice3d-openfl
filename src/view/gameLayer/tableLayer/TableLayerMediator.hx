package view.gameLayer.tableLayer;

import openfl.events.EventDispatcher;
import openfl.display.Sprite;
import openfl.events.Event;
import events.GameEvent;


/**
 * ...
 * @author Igor Skotnikov
 */
class TableLayerMediator
{
	/* VARS */
	private var tableLayer:TableLayer;
	
	private var eventDispatcher:EventDispatcher;
	private var contextview:Sprite;
	
	/* CONSTRUCTOR */
	public function new(tableLayer:TableLayer) 
	{
		this.tableLayer = tableLayer;		
		setupListeners();
	}
	
	private function setupListeners():Void {
		eventDispatcher = GlobalEventDispatcher.getInstance().eventDispatcher;
		eventDispatcher.addEventListener(GameEvent.DRAW_TRAECTORY, DRAW_TRAECTORY_HANDLER);
	}
	
	/* HANDLERS */
	
	private function DRAW_TRAECTORY_HANDLER(e:GameEvent):Void {
		tableLayer.parseTraectory(e.data);
	}
}