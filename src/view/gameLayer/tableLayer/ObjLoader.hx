package view.gameLayer.tableLayer;

/**
 * ...
 * @author Igor Skotnikov
 */
class ObjLoader {

	static var indexedVertices:Array<Float>;
	static var indexedUVs:Array<Float>;
	static var indexedNormals:Array<Float>;
	static var index:Int;

	public var data:Array<Float>;
	public var indices:Array<Int>;
	public var m_indexedVertices:Array<Float>;
	public var m_indexedUVs:Array<Float>;
	public var m_indexedNormals:Array<Float>;
	public var m_indeces:Array<Int>;
	
	private var pm_indexedVertices:Array<Float>;
	private var pm_indexedUVs:Array<Float>;
	private var pm_indexedNormals:Array<Float>;

	public function new(objData:String) {

		var vertices:Array<Float> = [];
		var uvs:Array<Float> = [];
		var normals:Array<Float> = [];

		var vertexIndices:Array<Int> = [];
		var uvIndices:Array<Int> = [];
		var normalIndices:Array<Int> = [];

		var tempVertices:Array<Array<Float>> = [];
		var tempUVs:Array<Array<Float>> = [];
		var tempNormals:Array<Array<Float>> = [];

		m_indexedVertices = [];
		m_indexedUVs = [];
		m_indexedNormals = [];
		pm_indexedVertices = [];
		pm_indexedUVs = [];
		pm_indexedNormals = [];
		
		var lines:Array<String> = objData.split("\n");
		var count = 0;
		for (i in 0...lines.length) {
			var words:Array<String> = lines[i].split(" ");

			if (words[0] == "v") {
				var vector:Array<Float> = [];
				vector.push(Std.parseFloat(words[1]));
				vector.push(Std.parseFloat(words[2]));
				vector.push(Std.parseFloat(words[3]));
				pm_indexedVertices.push(Std.parseFloat(words[1]));
				pm_indexedVertices.push(Std.parseFloat(words[2]));
				pm_indexedVertices.push(Std.parseFloat(words[3]));
				tempVertices.push(vector);
			}
			else if (words[0] == "vt") {
				var vector:Array<Float> = [];
				vector.push(Std.parseFloat(words[1]));
				vector.push(Std.parseFloat(words[2]));
				pm_indexedUVs.push(Std.parseFloat(words[1]));
				pm_indexedUVs.push(Std.parseFloat(words[2]));
				tempUVs.push(vector);
			}
			else if (words[0] == "vn") {
				count++;
				var vector:Array<Float> = [];
				vector.push(Std.parseFloat(words[1]));
				vector.push(Std.parseFloat(words[2]));
				vector.push(Std.parseFloat(words[3]));
				pm_indexedNormals.push(Std.parseFloat(words[1]));
				pm_indexedNormals.push(Std.parseFloat(words[2]));
				pm_indexedNormals.push(Std.parseFloat(words[3]));
				tempNormals.push(vector);
			}
			
			else if (words[0] == "f") {
				var sec1:Array<String> = words[1].split("/");
				var sec2:Array<String> = words[2].split("/");
				var sec3:Array<String> = words[3].split("/");
				count++;
				addFace(Std.int(Std.parseFloat(sec1[0])), Std.int(Std.parseFloat(sec2[0])), Std.int(Std.parseFloat(sec3[0])), Std.int(Std.parseFloat(sec1[1])),
				Std.int(Std.parseFloat(sec2[1])), Std.int(Std.parseFloat(sec3[1])), Std.int(Std.parseFloat(sec1[2])), Std.int(Std.parseFloat(sec2[2])), Std.int(Std.parseFloat(sec3[2])));
				
				vertexIndices.push(Std.int(Std.parseFloat(sec1[0])));
				vertexIndices.push(Std.int(Std.parseFloat(sec2[0])));
				vertexIndices.push(Std.int(Std.parseFloat(sec3[0])));

				uvIndices.push(Std.int(Std.parseFloat(sec1[1])));
				uvIndices.push(Std.int(Std.parseFloat(sec2[1])));
				uvIndices.push(Std.int(Std.parseFloat(sec3[1])));
				
				normalIndices.push(Std.int(Std.parseFloat(sec1[2])));
				normalIndices.push(Std.int(Std.parseFloat(sec2[2])));
				normalIndices.push(Std.int(Std.parseFloat(sec3[2])));
			}
		}

		for (i in 0...vertexIndices.length) {
			var vertex:Array<Float> = tempVertices[vertexIndices[i] - 1];
			var uv:Array<Float> = tempUVs[uvIndices[i] - 1];
			var normal:Array<Float> = tempNormals[normalIndices[i] - 1];

			vertices.push(vertex[0]);
			vertices.push(vertex[1]);
			vertices.push(vertex[2]);
			uvs.push(uv[0]);
			uvs.push(uv[1]);
			normals.push(normal[0]);
			normals.push(normal[1]);
			normals.push(normal[2]);
		}

		build(vertices, uvs, normals);

		data = [];
		for (i in 0...Std.int(vertices.length / 3)) {
			data.push(indexedVertices[i * 3]);
			data.push(indexedVertices[i * 3 + 1]);
			data.push(indexedVertices[i * 3 + 2]);
			data.push(indexedUVs[i * 2]);
			data.push(1-indexedUVs[i * 2 + 1]);
			data.push(indexedNormals[i * 3]);
			data.push(indexedNormals[i * 3 + 1]);
			data.push(indexedNormals[i * 3 + 2]);
		}
	}

	function build(vertices:Array<Float>, uvs:Array<Float>, normals:Array<Float>) {
		indexedVertices = [];
		indexedUVs = [];
		indexedNormals = [];
		indices = [];

		// For each input vertex
		for (i in 0...Std.int(vertices.length / 3)) {

			// Try to find a similar vertex in out_XXXX
			var found:Bool = getSimilarVertexIndex(
				vertices[i * 3], vertices[i * 3 + 1], vertices[i * 3 + 2],
				uvs[i * 2], uvs[i * 2 + 1],
				normals[i * 3], normals[i * 3 + 1], normals[i * 3 + 2]);

			if (found) { // A similar vertex is already in the VBO, use it instead !
				indices.push(index);
			}else{ // If not, it needs to be added in the output data.
				indexedVertices.push(vertices[i * 3]);
				indexedVertices.push(vertices[i * 3 + 1]);
				indexedVertices.push(vertices[i * 3 + 2]);
				indexedUVs.push(uvs[i * 2 ]);
				indexedUVs.push(uvs[i * 2 + 1]);
				indexedNormals.push(normals[i * 3]);
				indexedNormals.push(normals[i * 3 + 1]);
				indexedNormals.push(normals[i * 3 + 2]);
				indices.push(Std.int(indexedVertices.length / 3) - 1);
			}
		}
		//m_indexedNormals = vertices;
		//m_indexedVertices = indexedVertices;
		//m_indexedUVs = indexedUVs;
	}

	// Returns true if v1 can be considered equal to v2
	function isNear(v1:Float, v2:Float):Bool {
		return Math.abs(v1 - v2) < 0.01;
	}

	// Searches through all already-exported vertices for a similar one.
	// Similar = same position + same UVs + same normal
	function getSimilarVertexIndex( 
		vertexX:Float, vertexY:Float, vertexZ:Float,
		uvX:Float, uvY:Float,
		normalX:Float, normalY:Float, normalZ:Float
	):Bool {
		// Lame linear search
		for (i in 0...Std.int(indexedVertices.length / 3)) {
			if (
				isNear(vertexX, indexedVertices[i * 3]) &&
				isNear(vertexY, indexedVertices[i * 3 + 1]) &&
				isNear(vertexZ, indexedVertices[i * 3 + 2]) &&
				isNear(uvX    , indexedUVs     [i * 2]) &&
				isNear(uvY    , indexedUVs     [i * 2 + 1]) &&
				isNear(normalX, indexedNormals [i * 3]) &&
				isNear(normalY, indexedNormals [i * 3 + 1]) &&
				isNear(normalZ, indexedNormals [i * 3 + 2])
			) {
				index = i;
				return true;
			}
		}
		// No other vertex could be used instead.
		// Looks like we'll have to add it to the VBO.
		return false;
	}
	
	function addFace(a:Int, b:Int, c:Int, ua:Int, ub:Int, uc:Int, na:Int, nb:Int, nc:Int) {
        var ia = parseVertexIndex(a);
        var ib = parseVertexIndex(b);
        var ic = parseVertexIndex(c);		
        addVertex(ia, ib, ic);
		
        ia = parseUVIndex(ua);
        ib = parseUVIndex(ub);
        ic = parseUVIndex(uc);
        addUV(ia, ib, ic);
		
        ia = parseNormalIndex(na);
        ib = parseNormalIndex(nb);
        ic = parseNormalIndex(nc);
        addNormal(ia, ib, ic);
    }
	
	function parseVertexIndex(value:Int):Int {
        var index = Std.int(value);
        return Std.int(index >= 0 ? index - 1 : index + m_indexedVertices.length / 3) * 3;
    }
		
    function parseNormalIndex(value:Int):Int {
		var index = Std.int(value);
        return Std.int(index >= 0 ? index - 1 : index + m_indexedNormals.length / 3) * 3;
    }
	function parseUVIndex(value:Int):Int {
        var index = Std.int(value);
        return Std.int(index >= 0 ? index - 1 : index + m_indexedUVs.length / 2) * 2;
    }

    function addVertex(a:Int, b:Int, c:Int) {
		m_indexedVertices.push(pm_indexedVertices[ a ]);
		m_indexedVertices.push(pm_indexedVertices[ a + 1 ]);
		m_indexedVertices.push(pm_indexedVertices[ a + 2 ]);
        m_indexedVertices.push(pm_indexedVertices[ b ]);
		m_indexedVertices.push(pm_indexedVertices[ b + 1 ]);
		m_indexedVertices.push(pm_indexedVertices[ b + 2 ]);
		m_indexedVertices.push(pm_indexedVertices[ c ]);
		m_indexedVertices.push(pm_indexedVertices[ c + 1 ]);
		m_indexedVertices.push(pm_indexedVertices[ c + 2 ]);
    }

    function addNormal(a:Int, b:Int, c:Int) {
        m_indexedNormals.push(pm_indexedNormals[ a ]);
		m_indexedNormals.push(pm_indexedNormals[ a + 1 ]);
		m_indexedNormals.push(pm_indexedNormals[ a + 2 ]);
        m_indexedNormals.push(pm_indexedNormals[ b ]);
		m_indexedNormals.push(pm_indexedNormals[ b + 1 ]);
		m_indexedNormals.push(pm_indexedNormals[ b + 2 ]);
		m_indexedNormals.push(pm_indexedNormals[ c ]);
		m_indexedNormals.push(pm_indexedNormals[ c + 1 ]);
		m_indexedNormals.push(pm_indexedNormals[ c + 2 ]);
	}

    function addUV(a:Int, b:Int, c:Int) {
        m_indexedUVs.push(pm_indexedUVs[ a ]);
		m_indexedUVs.push(pm_indexedUVs[ a + 1 ]);
        m_indexedUVs.push(pm_indexedUVs[ b ]);
		m_indexedUVs.push(pm_indexedUVs[ b + 1 ]);
		m_indexedUVs.push(pm_indexedUVs[ c ]);
		m_indexedUVs.push(pm_indexedUVs[ c + 1 ]);
    }
}