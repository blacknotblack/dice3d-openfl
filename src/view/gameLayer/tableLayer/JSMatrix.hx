package view.gameLayer.tableLayer;
import openfl.geom.Matrix3D;
import openfl.Vector;

/**
 * ...
 * @author Igor Skotnikov
 */
class JSMatrix extends Matrix3D
{

	public function new(v:Vector<Float>=null) 
	{
		super(v);
	}
	
	public function multiply(matrix:JSMatrix):JSMatrix {
		var e:Vector<Float>;
		var a:Vector<Float>;
		var b:Vector<Float>;
		var ai0:Float;
		var ai1:Float;
		var ai2:Float;
		var ai3:Float;
		
		e = this.rawData;
		a = this.rawData;
		b = matrix.rawData;
		
		for (i in 0...4) {
			ai0 = a[i];
			ai1 = a[i + 4];
			ai2 = a[i + 8];
			ai3 = a[i + 12];
			e[i] 	= ai0 * b[0] + ai1 * b[1] + ai2 * b[2] + ai3 * b[3];
			e[i+4] 	= ai0 * b[4] + ai1 * b[5] + ai2 * b[6] + ai3 * b[7];
			e[i+8] 	= ai0 * b[8] + ai1 * b[9] + ai2 * b[10]+ ai3 * b[11];
			e[i+12] = ai0 * b[12]+ ai1 * b[13]+ ai2 * b[14]+ ai3 * b[15];
		}
		return this;
	}
	
	public function lookAtFloat(eyeX:Float, eyeY:Float, eyeZ:Float, centerX:Float, centerY:Float, centerZ:Float, upX:Float, upY:Float, upZ:Float):JSMatrix {
		return this.multiply(new JSMatrix().setLookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ));
	}
	
	public function setPerspective(fovy:Float, aspect:Float, near:Float, far:Float):JSMatrix {
		var rd:Float;
		var s:Float;
		var ct:Float;

		if (near == far || aspect == 0) {
			throw 'null frustum';
		}
		if (near <= 0) {
			throw 'near <= 0';
		}
		if (far <= 0) {
			throw 'far <= 0';
		}

		fovy = Math.PI * fovy / 180 / 2;
		s = Math.sin(fovy);
		if (s == 0) {
			throw 'null frustum';
		}

		rd = 1 / (far - near);
		ct = Math.cos(fovy) / s;

		this.rawData[0]  = ct / aspect;
		this.rawData[1]  = 0;
		this.rawData[2]  = 0;
		this.rawData[3]  = 0;

		this.rawData[4]  = 0;
		this.rawData[5]  = ct;
		this.rawData[6]  = 0;
		this.rawData[7]  = 0;

		this.rawData[8]  = 0;
		this.rawData[9]  = 0;
		this.rawData[10] = -(far + near) * rd;
		this.rawData[11] = -1;

		this.rawData[12] = 0;
		this.rawData[13] = 0;
		this.rawData[14] = -2 * near * far * rd;
		this.rawData[15] = 0;

		return this;
	};
	
	private function setLookAt(eyeX:Float, eyeY:Float, eyeZ:Float, centerX:Float, centerY:Float, centerZ:Float, upX:Float, upY:Float, upZ:Float):JSMatrix {
		//var e, 
		var fx:Float;
		var fy:Float;
		var fz:Float;
		var rlf:Float;
		var sx:Float;
		var sy:Float;
		var sz:Float;
		var rls:Float;
		var ux:Float;
		var uy:Float;
		var uz:Float;
		
		fx = centerX - eyeX;
		fy = centerY - eyeY;
		fz = centerZ - eyeZ;
		
		rlf = 1 / Math.sqrt(fx * fx + fy * fy + fz * fz);
		fx *= rlf;
		fy *= rlf;
		fz *= rlf;
		
		sx = fy * upZ - fz * upY;
		sy = fz * upX - fz * upZ;
		sz = fx * upY - fy * upX;
		
		rls = 1 / Math.sqrt(sx*sx + sy*sy + sz*sz);
		sx *= rls;
		sy *= rls;
		sz *= rls;

		ux = sy * fz - sz * fy;
		uy = sz * fx - sx * fz;
		uz = sx * fy - sy * fx;

		this.rawData[0] = sx;
		this.rawData[1] = ux;
		this.rawData[2] = -fx;
		this.rawData[3] = 0;
		this.rawData[4] = sy;
		this.rawData[5] = uy;
		this.rawData[6] = -fy;
		this.rawData[7] = 0;

		this.rawData[8] = sz;
		this.rawData[9] = uz;
		this.rawData[10] = -fz;
		this.rawData[11] = 0;

		this.rawData[12] = 0;
		this.rawData[13] = 0;
		this.rawData[14] = 0;
		this.rawData[15] = 1;
		
		return this.translate(-eyeX, -eyeY, -eyeZ);
	}
	
	public function translate(x:Float, y:Float, z:Float):JSMatrix {
		this.rawData[12] += this.rawData[0] * x + this.rawData[4] * y + this.rawData[8]  * z;
		this.rawData[13] += this.rawData[1] * x + this.rawData[5] * y + this.rawData[9]  * z;
		this.rawData[14] += this.rawData[2] * x + this.rawData[6] * y + this.rawData[10] * z;
		this.rawData[15] += this.rawData[3] * x + this.rawData[7] * y + this.rawData[11] * z;
		return this;
	}
	
	override public function clone ():JSMatrix {
		
		return new JSMatrix (this.rawData.copy ());
		
	}
}