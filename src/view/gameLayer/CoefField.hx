package view.gameLayer;
import openfl.display.Sprite;
import openfl.text.TextField;
import openfl.text.TextFormat;

/**
 * ...
 * @author Igor Skotnikov
 */
class CoefField extends TextField
{
	//private var tf_coef:TextField;
	private var mediator:CoefFieldMediator;
	public function new() 
	{
		super();
		//tf_coef = new TextField();
		defaultTextFormat = new TextFormat(GameAssets.getInstance().fontRobotoRegular.fontName, 20, 0xffffff, false, false, false, null, null, "right");
		embedFonts = true;
		height = 30;
		width = 80;
		//border = true;
		borderColor = 0xffffff;
		//addChild(tf_coef);
		mediator = new CoefFieldMediator(this);
	}
	
	public function setKoef(value:String):Void {
		text = "x "+value;
	}
	
}