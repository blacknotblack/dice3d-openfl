package view.gameLayer.statLayer;

import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import openfl.text.TextField;
import openfl.text.TextFormat;

/**
 * ...
 * @author Igor Skotnikov
 */
class BaseStatButton extends Sprite
{

	public var isSelected:Bool;
	
	public var statMode(default, null):EStats;
	
	private var mediator:BaseStatButtonMediator;
	
	private var up:Bitmap;
	//private var	over:Bitmap;
	private var down:Bitmap;
	private var tf:TextField;
	
	public function new(text:String, textSize:Int=18, upURL:String, downURL:String, flip:Bool=false)
	{
		super();
		scaleX = scaleY = 0.82;
		mediator = new BaseStatButtonMediator(this);
		up = new Bitmap(Assets.getBitmapData(upURL), null, true);
		down = new Bitmap(Assets.getBitmapData(downURL), null, true);
		//over = new Bitmap(Assets.getBitmapData(overURL), null, true);
		if (flip) {
			up.scaleX = down.scaleX = -1;
			up.x = up.width;
			down.x = down.width;
		}
		addChild(up);
		
		tf 						= new TextField();
		tf.defaultTextFormat	= new TextFormat(GameAssets.getInstance().fontRobotoRegular.fontName, textSize, 0xffffff, null, null, null, null, null, "center");
		tf.embedFonts 			= true;
		tf.selectable 			= false;
		tf.multiline 			= false;
		tf.wordWrap 			= false;
		tf.width				= up.width - 30;
		tf.height				= 40;
		//tf.border				= true;
		//tf.borderColor 		= 0xffff;
		tf.text					= text;
		while (tf.width < tf.textWidth + 10) {
			tf.defaultTextFormat	= new TextFormat(GameAssets.getInstance().fontRobotoRegular.fontName, textSize--, 0xffffff, null, null, null, null, null, "center");
			tf.text = text;
		}
		var bmd:BitmapData = new BitmapData(Std.int(tf.width), Std.int(tf.height), false, 0xfff);
		bmd.draw(tf);
		//tf.height = tf.textHeight;
		tf.y = (up.height - tf.height) / 2+5;
		tf.x = (up.width - tf.width) / 2;
		addChild(tf);
	}
	
	public function switchState(state:String):Void {
		removeChildAt(0);
		switch(state) {
			case "up": 		addChildAt(up, 0); 		isSelected = false; 		/* tf.textColor = 0x0; */
			case "down":	addChildAt(down, 0); 	isSelected = true;			/* tf.textColor = 0xffffff; */
			//case "over":	if (isSelected) { addChildAt(down, 0); /* tf.textColor = 0xffffff; */} else { addChildAt(over, 0);  /* tf.textColor = 0x0; */}
			//case "out":		if (isSelected) { addChildAt(down, 0); /* tf.textColor = 0xffffff; */} else { addChildAt(up, 0);    /* tf.textColor = 0x0; */}
		}
	}
}