package view.gameLayer.statLayer;

import openfl.display.Sprite;
import openfl.events.KeyboardEvent;
import view.gameLayer.statLayer.mystat.MyStat;

/**
 * ...
 * @author Igor Skotnikov
 */
class StatLayer extends Sprite
{
	private var mediator:StatLayerMediator;
	
	private var myBetsButton:MyBetsButton;
	private var allBetsButton:AllBetsButton;
	
	private var myStat:MyStat;
	private var allStat:MyStat;
	public function new() 
	{
		super();
		
		mediator = new StatLayerMediator(this);
		
		allBetsButton = new AllBetsButton();
		addChild(allBetsButton);
		myBetsButton = new MyBetsButton();
		myBetsButton.x = allBetsButton.width;
		addChild(myBetsButton);
		
		myStat = new MyStat("Throw");
		myStat.y = myBetsButton.height + 20;
		
		allStat = new MyStat("Name");
		allStat.y = myStat.y;
		addChild(allStat);
		
		switchStat(EStats.ALL_STATS);
		Context.getInstance().contextview.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKey);
	}
	
	public function switchStat(statType:EStats):Void {
		switch(statType) {
			case EStats.ALL_STATS:
				allBetsButton.switchState("down"); myBetsButton.switchState("up"); removeChildAt(numChildren - 1); addChild(allStat);
			case EStats.MY_STATS:
				allBetsButton.switchState("up"); myBetsButton.switchState("down"); removeChildAt(numChildren - 1); addChild(myStat);
		}
	}
	
	function onKey(e:KeyboardEvent) {
		myStat.addElement({name:"Durak", bet:"100", coef:"35.64", gameDescription:{gameType:1, dice:"9"}, roll:"9", profit:"+3564.00"});
	}
	
}