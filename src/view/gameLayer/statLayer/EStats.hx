package view.gameLayer.statLayer;

/**
 * @author Igor Skotnikov
 */
enum EStats 
{
	MY_STATS;
	ALL_STATS;
}