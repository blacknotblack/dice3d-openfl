package view.gameLayer.statLayer.mystat;

import events.GameEvent;

/**
 * ...
 * @author Igor Skotnikov
 */
class MyStatMediator
{
	private var view:MyStat;
	
	public function new(view:MyStat) 
	{
		this.view = view;
		init();
	}
	
	inline function init():Void {
		GlobalEventDispatcher.getInstance().eventDispatcher.addEventListener(GameEvent.UPDATE_MYSTATS, UPDATE_MYSTATS_HANDLER);
	}
	
	private function UPDATE_MYSTATS_HANDLER(e:GameEvent):Void {
		view.addElement(e.data);
	}
	
	public function dispose():Void {
		GlobalEventDispatcher.getInstance().eventDispatcher.removeEventListener(GameEvent.UPDATE_MYSTATS, UPDATE_MYSTATS_HANDLER);
	}
	
}