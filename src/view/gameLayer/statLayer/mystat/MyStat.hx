package view.gameLayer.statLayer.mystat;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import data.multiplayer.GameModel;
import view.gameLayer.statLayer.StatElement;
import motion.Actuate;
import openfl.Assets;

/**
 * ...
 * @author Igor Skotnikov
 */
class MyStat extends Sprite
{
	private var _mask:Bitmap;
	private var _stats:Array<StatElement>;
	private var mediator:MyStatMediator;
	private var title:StatElement;
	public function new(name:String) 
	{
		super();
		_stats = [];
		mediator = new MyStatMediator(this);
		
		title = new StatElement(name, "Bet", "Multiplayer", "Game", "Number", "Income", 17);
		addChild(title);
		
		_mask = new Bitmap(Assets.getBitmapData("assets/img/stats/mask.png"));
		_mask.scaleY = 80;
		_mask.y = title.height;
		//addChild(_mask);
	}
	
	public function addElement(data:Dynamic):Void {
		var gameDesc:String = "";
		switch(data.gameDescription.gameType) {
			case GameModel.OVER:
				gameDesc = Localization.getInstance().data.underGameTypeButton;
			case GameModel.UNDER:
				gameDesc = Localization.getInstance().data.overGameTypeButton;
			case GameModel.EQUAL:
				gameDesc = Localization.getInstance().data.equalGameTypeButton;
			case GameModel.DOUBLE:
				gameDesc = Localization.getInstance().data.doubleGameTypeButton;
			case GameModel.ALLDOUBLES:
				gameDesc = Localization.getInstance().data.allDoubleGameTypeButton;
		}
		gameDesc += " " + data.gameDescription.dice;
		_stats.unshift(new StatElement(data.name, Std.string(data.bet), Std.string(data.coef), gameDesc, Std.string(data.roll), Std.string(data.profit)));
		organizeView();
	}
	
	private function organizeView():Void {
		if (_mask.parent == null) addChild(_mask);
		_stats[0].y = -_stats[0].height + title.height;
		_stats[0].mask = _mask;
		addChild(_stats[0]);
		for(i in 0..._stats.length)
			Actuate.tween(_stats[i], 0.3, { y:(_stats[0].height+10) * i + title.height+10 } );
		Actuate.timer(0.3).onComplete(function(){
			while (_stats.length > 10) {
				removeChild(_stats[_stats.length - 1]);
				var _s = _stats.pop();
				_s = null;
			}
		});
	}
	
	public function dispose():Void {
		mediator.dispose();
		_stats = [];
	}
}