package view.gameLayer.statLayer;

/**
 * ...
 * @author Igor Skotnikov
 */
class MyBetsButton extends BaseStatButton
{

	public function new() 
	{
		super(Localization.getInstance().data.myBetsButtonLabel, "assets/img/stats/deactive.png", "assets/img/stats/active.png", true);
		statMode = EStats.MY_STATS;
	}
	
}