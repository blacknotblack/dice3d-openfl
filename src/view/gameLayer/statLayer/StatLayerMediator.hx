package view.gameLayer.statLayer;

import openfl.events.EventDispatcher;
import openfl.display.Sprite;
import openfl.events.Event;
import events.GameEvent;

/**
 * ...
 * @author Igor Skotnikov
 */
class StatLayerMediator
{
	/* VARS */
	private var view:StatLayer;
	
	/* CONSTRUCTOR */
	public function new(view:StatLayer) 
	{
		this.view = view;	
		setupListeners();
	}
	
	private function setupListeners():Void {
		GlobalEventDispatcher.getInstance().eventDispatcher.addEventListener(GameEvent.STAT_TYPE_BUTTON_SELECTED, STAT_TYPE_BUTTON_SELECTED_HANDLER);
	}
	
	/* HANDLERS */
	
	private function STAT_TYPE_BUTTON_SELECTED_HANDLER(e:GameEvent):Void { 
		view.switchStat(e.data.statType);
	}
	
	
	/* API */
	
}