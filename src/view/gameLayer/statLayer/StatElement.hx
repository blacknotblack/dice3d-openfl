package view.gameLayer.statLayer;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import openfl.text.TextField;
import openfl.text.TextFormat;

/**
 * ...
 * @author Igor Skotnikov
 */
class StatElement extends Sprite
{
	private var userName:TextField;
	private var bet:TextField;
	private var coef:TextField;
	private var game:TextField;
	private var roll:TextField;
	private var profit:TextField;
	private var divider:Bitmap;
	public function new(_name:String="", _bet:String="", _coef:String="", _game:String="", _roll:String="", _profit:String="", size:Int = 14) 
	{
		super();
		userName = new TextField();
		userName.defaultTextFormat = new TextFormat(GameAssets.getInstance().fontRobotoRegular.fontName, size, 0xffffff, null, null, null, null, null, "center");
		userName.height = 20;
		userName.width = 120;
		//userName.border = true;
		userName.text = _name;
		addChild(userName);
		
		bet = new TextField();
		bet.defaultTextFormat = new TextFormat(GameAssets.getInstance().fontRobotoRegular.fontName, size, 0xffffff, null, null, null, null, null, "center");
		bet.height = 20;
		bet.width = 100;
		//bet.border = true;
		bet.x = userName.width;
		bet.text = _bet;
		addChild(bet);
		
		coef = new TextField();
		coef.defaultTextFormat = new TextFormat(GameAssets.getInstance().fontRobotoRegular.fontName, size, 0xffffff, null, null, null, null, null, "center");
		coef.height = 20;
		coef.width = 100;
		//coef.border = true;
		coef.x = bet.x + bet.width;
		coef.text = _coef;
		addChild(coef);
		
		game = new TextField();
		game.defaultTextFormat = new TextFormat(GameAssets.getInstance().fontRobotoRegular.fontName, size, 0xffffff, null, null, null, null, null, "center");
		game.height = 20;
		game.width = 100;
		//game.border = true;
		game.x = coef.x + coef.width;
		game.text = _game;
		addChild(game);
		
		roll = new TextField();
		roll.defaultTextFormat = new TextFormat(GameAssets.getInstance().font.fontName, size, 0xffffff, null, null, null, null, null, "center");
		roll.height = 20;
		roll.width = 100;
		//roll.border = true;
		roll.x = game.x+game.width;
		roll.text = _roll;
		addChild(roll);
		
		profit = new TextField();
		profit.defaultTextFormat = new TextFormat(GameAssets.getInstance().font.fontName, size, 0xffffff, null, null, null, null, null, "center");
		profit.height = 20;
		profit.width = 120;
		//profit.border = true;
		profit.x = roll.x + roll.width;
		profit.text = _profit;
		addChild(profit);
		
		divider = new Bitmap(GameAssets.getInstance().DIVIDER);
		divider.scaleX = divider.scaleY = 0.82;
		divider.y = 30;
		addChild(divider);
	}
	
}