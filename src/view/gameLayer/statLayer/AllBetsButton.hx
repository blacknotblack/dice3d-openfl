package view.gameLayer.statLayer;

/**
 * ...
 * @author Igor Skotnikov
 */
class AllBetsButton extends BaseStatButton
{

	public function new() 
	{
		super(Localization.getInstance().data.allBetsButtonLabel, "assets/img/stats/deactive.png", "assets/img/stats/active.png");
		statMode = EStats.ALL_STATS;
	}
	
}