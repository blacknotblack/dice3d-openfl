package view.gamelayer;

import openfl.display.Bitmap;
import openfl.display.Sprite;
import view.gameLayer.buttonsLayer.ButtonsLayer;
import view.gameLayer.CoefField;
import view.gameLayer.statLayer.mystat.MyStat;
import view.gameLayer.statLayer.StatElement;
import view.gameLayer.statLayer.StatLayer;
import view.gameLayer.tableLayer.TableLayer;
import openfl.display.PixelSnapping;

/**
 * ...
 * @author Igor Skotnikov	
 */
class GameLayer extends Sprite
{
	private var background:Bitmap;
	private var gameLayerMediator:GameLayerMediator;
	private var buttonLayer:ButtonsLayer;
	//private var coefField:CoefField;
	private var tableLayer:TableLayer;
	
	private var statLayer:StatLayer;
	public function new() 
	{
		super();
		setupBackground();
		//y = 40;
		
		tableLayer = new TableLayer();
		addChild(tableLayer);
		
		buttonLayer = new ButtonsLayer();
		buttonLayer.y = 800;
		addChild(buttonLayer);

		statLayer = new StatLayer();
		statLayer.x = 50;
		statLayer.y = 100;
		addChild(statLayer);
		
		gameLayerMediator = new GameLayerMediator(this);
	}
	
	public function resize(width:Float, height:Float):Void {
		var factorX:Float = width / Main.WIDTH;
		var factorY:Float = height / Main.HEIGHT;
		var factor:Float = (factorX < factorY)?factorX:factorY;
		factor = (factor < 0.7)? 0.7:factor;
		buttonLayer.scaleX = buttonLayer.scaleY = factor;
		buttonLayer.y = height - buttonLayer.height - 10 * factor;
		if (buttonLayer.y < 400) buttonLayer.y = 400;
		
		statLayer.x = 50*factor;
		statLayer.y = 100*factor;
		statLayer.scaleX = statLayer.scaleY = factor;
		tableLayer.resize(width, height, buttonLayer.y);
		
		factor = (factorX > factorY)?factorX:factorY;
		background.scaleX = background.scaleY = factor;
	}
	
	
	private function setupBackground():Void {
		background = new Bitmap(GameAssets.getInstance().BACKGROUND, PixelSnapping.ALWAYS, true);
		background.y = 40;
		addChild(background);
	}
}