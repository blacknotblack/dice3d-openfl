package view.gameLayer;

import data.multiplayer.GameModel;
import data.multiplayer.NumberFieldModel;
import events.GameEvent;
import data.multiplayer.Room;

/**
 * ...
 * @author Igor Skotnikov
 */
class CoefFieldMediator
{
	var view:CoefField;
	var model:Array<Int> = [2, 3, 4, 5, 6, 7,
							3, 4, 5, 6, 7, 8,
							4, 5, 6, 7, 8, 9,
							5, 6, 7, 8, 9, 10,
							6, 7, 8, 9, 10, 11,
							7, 8, 9, 10, 11, 12];
									
	public function new(view:CoefField) 
	{
		this.view = view;
		GlobalEventDispatcher.getInstance().eventDispatcher.addEventListener(GameEvent.UPDATE_MULTIPLICATOR, UPDATE_MULTIPLICATOR_HANDLER);
		UPDATE_MULTIPLICATOR_HANDLER(null);
	}	
	
	private function UPDATE_MULTIPLICATOR_HANDLER(e:GameEvent):Void {
		var multi:Int = 0;
		switch(GameModel.getInstance().gameType) {
			case GameModel.OVER:
				for (i in 0...36)
					if (model[i] > NumberFieldModel.getInstance().number) multi++;
			case GameModel.UNDER:
				for (i in 0...36)
					if (model[i] < NumberFieldModel.getInstance().number) multi++;
			case GameModel.EQUAL:
				for (i in 0...36)
					if (model[i] == NumberFieldModel.getInstance().number) multi++;
			case GameModel.DOUBLE:
				multi = 1;
			case GameModel.ALLDOUBLES:
				multi = 6;
		}
		view.setKoef("" + Std.int(36 / (multi * Room.getInstance().multiplicator) * 100) / 100);
	}
}