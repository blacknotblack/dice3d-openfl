package view.topbar.button;
import events.ApplicationEvent;
import govnoknopka.button.Button;
import openfl.events.MouseEvent;

/**
 * ...
 * @author Igor Skotnikov
 */
class FullScreenButtonMediator
{

	var view:Button;
	var fullsreenstate:Bool;
	public function new(view:Button) 
	{
		this.view = view;
		init();
	}
	
	inline private function init() {
		fullsreenstate = false;
		view.addEventListener(MouseEvent.MOUSE_UP, MOUSE_HANDLER);
	}
	
	private function MOUSE_HANDLER(e:MouseEvent) {
		if (fullsreenstate)	fullsreenstate = false;
		else 				fullsreenstate = true;
		GlobalEventDispatcher.getInstance().dispatch(new ApplicationEvent(ApplicationEvent.FULL_SCREEN, {fullscreenstate:fullsreenstate}));
	}
}