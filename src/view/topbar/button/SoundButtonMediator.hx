package view.topbar.button;
import events.ApplicationEvent;
import govnoknopka.button.Button;
import openfl.events.MouseEvent;

/**
 * ...
 * @author Igor Skotnikov
 */
class SoundButtonMediator
{

	var view:SoundButton;
	var soundOn:Bool;
	public function new(view:SoundButton) 
	{
		this.view = view;
		init();
	}
	
	inline private function init() {
		soundOn = true;
		view.addEventListener(MouseEvent.MOUSE_UP, MOUSE_HANDLER);
	}
	
	private function MOUSE_HANDLER(e:MouseEvent) {
		soundOn = !soundOn;
		view.setIcons(soundOn);
		//GlobalEventDispatcher.getInstance().dispatch(new ApplicationEvent(ApplicationEvent.FULL_SCREEN, {fullscreenstate:fullsreenstate}));
	}
}