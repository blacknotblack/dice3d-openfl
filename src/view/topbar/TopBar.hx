package view.topbar;
import data.manager.timeoutManager.TimeoutManager;
import govnoknopka.layout.HGroup;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import openfl.text.AntiAliasType;
import openfl.text.TextField;
import openfl.text.TextFieldAutoSize;
import openfl.text.TextFormat;
import view.topbar.button.FullScreenButton;
import view.topbar.button.LeaveRoomButton;
import view.topbar.button.SoundButton;

/**
 * ...
 * @author Igor Skotnikov
 */
class TopBar extends Sprite
{
	private var bg:Bitmap;
	private var rootLayout:HGroup;
	private var leftLayout:HGroup;
	private var rightLayout:HGroup;
	
	private var fullScreenButton:FullScreenButton;
	private var soundButton:SoundButton;
	private var leaveRoomButton:LeaveRoomButton;
	
	private var _balanceLabelTextField:TextField;
	private var _balanceTextField:TextField;
	private var _bankLabelTextField:TextField;
	private var _bankTextField:TextField;
	private var _currency:String;
	private var _gameIdTextField:TextField;
	
	private var _bank:Float;
	private var _balance:Float;
	
	private var _animateBalanceInterval	:String;
	private var _animateBankInterval	:String;
	
	private var mediator:TopBarMediator;
	
	public function new() 
	{
		super();
		
		_balance = 0;
		_bank = 0;
		
		mediator = new TopBarMediator(this);
		drawBG();
		drawLayouts();
		drawButtons();
		drawTextFields();
	}
	
	private function drawBG():Void {
		bg = new Bitmap(Assets.getBitmapData("assets/img/topbar/topbarBg/topBarBg.png"));
		bg.height = 40;
		addChild(bg);
	}
	
	private function drawLayouts():Void {
		rootLayout = new HGroup("root", "right", 5);
		rightLayout = new HGroup("r", "right");
		leftLayout = new HGroup("l", "left");
		rootLayout.layout(this);
		//rootLayout.setSize(400, 40);
	}
	
	private function drawTextFields():Void {
		_currency = AppData.getInstance().currency != "" ? " " + AppData.getInstance().currency : "";
		
		_balanceLabelTextField 						= new TextField();
		_balanceLabelTextField.defaultTextFormat	= new TextFormat(GameAssets.getInstance().font.fontName, 12, 0xFFFFFF, null, null, null, null, null, "center");
		_balanceLabelTextField.embedFonts 			= true;
		_balanceLabelTextField.selectable 			= false;
		_balanceLabelTextField.multiline 			= false;
		_balanceLabelTextField.wordWrap 			= false;
		_balanceLabelTextField.antiAliasType 		= AntiAliasType.ADVANCED;
		_balanceLabelTextField.autoSize				= TextFieldAutoSize.LEFT;
		_balanceLabelTextField.sharpness			= 100;
		_balanceLabelTextField.width 				= _balanceLabelTextField.textWidth;
		_balanceLabelTextField.text					= Localization.getInstance().data.balanceLabel;
		
		_balanceTextField 						= new TextField();
		_balanceTextField.defaultTextFormat		= new TextFormat(GameAssets.getInstance().font.fontName, 12, 0xFFFFFF, null, null, null, null, null, "center");
		_balanceTextField.embedFonts 			= true;
		_balanceTextField.selectable 			= false;
		_balanceTextField.multiline 			= false;
		_balanceTextField.wordWrap 				= false;
		_balanceTextField.antiAliasType 		= AntiAliasType.ADVANCED;
		_balanceTextField.autoSize				= TextFieldAutoSize.LEFT;
		_balanceTextField.sharpness				= 100;
		_balanceTextField.width 				= _balanceTextField.textWidth;
		_balanceTextField.text					= "0" + _currency;
		
		_bankLabelTextField 					= new TextField();
		_bankLabelTextField.defaultTextFormat	= new TextFormat(GameAssets.getInstance().font.fontName, 12, 0xFFFFFF, null, null, null, null, null, "center");
		_bankLabelTextField.embedFonts 			= true;
		_bankLabelTextField.selectable 			= false;
		_bankLabelTextField.multiline 			= false;
		_bankLabelTextField.wordWrap 			= false;
		_bankLabelTextField.antiAliasType 		= AntiAliasType.ADVANCED;
		_bankLabelTextField.autoSize			= TextFieldAutoSize.LEFT;
		_bankLabelTextField.sharpness			= 100;
		_bankLabelTextField.width				= _bankLabelTextField.textWidth;
		_bankLabelTextField.text				= Localization.getInstance().data.bankLabel;
		
		_bankTextField 						= new TextField();
		_bankTextField.defaultTextFormat	= new TextFormat(GameAssets.getInstance().font.fontName, 12, 0xFFFFFF, null, null, null, null, null, "center");
		_bankTextField.embedFonts 			= true;
		_bankTextField.selectable 			= false;
		_bankTextField.multiline 			= false;
		_bankTextField.wordWrap 			= false;
		_bankTextField.antiAliasType 		= AntiAliasType.ADVANCED;
		_bankTextField.autoSize				= TextFieldAutoSize.LEFT;
		_bankTextField.sharpness			= 100;
		_bankTextField.width				= _bankTextField.textWidth;
		_bankTextField.text					= "0" + _currency;
		
		_gameIdTextField 					= new TextField();
		_gameIdTextField.defaultTextFormat	= new TextFormat(GameAssets.getInstance().font.fontName, 12, 0xFFFFFF, null, null, null, null, null, "center");
		_gameIdTextField.embedFonts 		= true;
		_gameIdTextField.selectable 		= false;
		_gameIdTextField.multiline 			= false;
		_gameIdTextField.wordWrap 			= false;
		_gameIdTextField.antiAliasType 		= AntiAliasType.ADVANCED;
		_gameIdTextField.autoSize			= TextFieldAutoSize.LEFT;
		_gameIdTextField.sharpness			= 100;
		_gameIdTextField.width				= _gameIdTextField.textWidth;
		_gameIdTextField.text				= Localization.getInstance().data.gameIDLabel;
		
		leftLayout.add(_balanceLabelTextField);
		leftLayout.add(_balanceTextField);
		leftLayout.add(_bankLabelTextField);
		leftLayout.add(_bankTextField);
		leftLayout.add(_gameIdTextField);
		rootLayout.add(leftLayout);
		rootLayout.layout(this);
	}
	
	private function drawButtons():Void {
		fullScreenButton = new FullScreenButton();
		soundButton = new SoundButton();
		leaveRoomButton = new LeaveRoomButton();
		rightLayout.add(leaveRoomButton);
		rightLayout.add(soundButton);
		rightLayout.add(fullScreenButton);
		rootLayout.add(rightLayout);
		rootLayout.layout(this);
	}
	
	public function resize(width:Float):Void {
		rootLayout.setSize(width, 40);
		bg.width = width;
	}
	
	/* UTILS */
	private function parseAmount(amount:Float):String{
		var amountS	:String = Std.string(amount);
		var i		:Int 	= amountS.length;
		var inc		:Int 	= 0;
		var result	:String = '';
		while(i>0){
			result = amountS.charAt(i-1)+result;
			inc++;
			if(inc==3){
				result 	= ' '+result;
				inc 	= 0;
			}
			i--;
		}
		return result;
	}
	
	public function setGameID(gameID:Float):Void{
		_gameIdTextField.text	= Localization.getInstance().data.gameIDLabel + " "+gameID;
		
		rootLayout.layout(this);
	}
	
	public function setBank(amount:Float):Void{
		_bank 					= amount;
		_bankTextField.text 	= parseAmount(amount) + _currency;
		
		rootLayout.layout(this);
	}
	public function animateBank(amount:Float):Void{
		var duration:Float = _bank < amount ? 3000 : 500;
		animateBankAmount(_bank, amount, duration);
	}
	
	public function setBalance(amount:Float):Void{
		_balance 				= amount;
		_balanceTextField.text 	= parseAmount(amount) + _currency;
		
		rootLayout.layout(this);
	}
	public function animateBalance(amount:Float):Void {
		var duration:Float = _balance < amount ? 3000 : 500;
		animateBalanceAmount(_balance, amount, duration);
	}
	private function animateBalanceAmount(from:Float, to:Float, duration:Float=3000):Void{
		if(from == to) return;
		
		TimeoutManager.getInstance().clearInterval(_animateBalanceInterval);
		
		var progress	:Float = from;
		var difference 	:Float = to - from;
		var step	   	:Float = Math.ceil(difference / (duration / 50));
		
		function update():Void{
			progress += step;
			
			if((from > to && progress <= to) || (from < to && progress >= to) || step == 0){
				progress = to;
				TimeoutManager.getInstance().clearInterval(_animateBalanceInterval);
			}
			setBalance(progress);
		}
		_animateBalanceInterval = TimeoutManager.getInstance().setInterval(50, update);
	}
	private function animateBankAmount(from:Float, to:Float, duration:Float=3000):Void{
		if(from == to) return;
		
		TimeoutManager.getInstance().clearInterval(_animateBankInterval);
		
		var progress	:Float = from;
		var difference 	:Float = to - from;
		var step	   	:Float = Math.ceil(difference / (duration / 50));
		
		function update():Void{
			progress += step;
			
			if((from > to && progress <= to) || (from < to && progress >= to || step == 0)){
				progress = to;
				TimeoutManager.getInstance().clearInterval(_animateBankInterval);
			}
			setBank(progress);
		}
		_animateBankInterval = TimeoutManager.getInstance().setInterval(50, update);
	}
}