package view.topbar.skin;

import govnoknopka.skin.SkinButton;
import openfl.Assets;
import openfl.display.Bitmap;

/**
 * ...
 * @author Igor Skotnikov
 */
class BlackSkinButton extends SkinButton
{
	
	public function new() 
	{
		super();
		skinOver = new Bitmap(Assets.getBitmapData("assets/img/topbar/button/BlackButtonOverAsset.png"));
		skinUp = new Bitmap(Assets.getBitmapData("assets/img/topbar/button/BlackButtonUpAsset.png"));
		skinDown = new Bitmap(Assets.getBitmapData("assets/img/topbar/button/BlackButtonDownAsset.png"));
	}
	
}