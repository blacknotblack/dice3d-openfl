package view.riskLayer.selectRiskDice;

import events.GameEvent;
import openfl.events.MouseEvent;

/**
 * ...
 * @author Igor Skotnikov
 */
class SelectRiskDiceLayerMediator
{
	private var view:SelectRiskDiceLayer;
	
	//inline private var totalCount:Int = 3;
	//private var currentCount:Int = 0;
	
	public function new(view:SelectRiskDiceLayer) 
	{
		this.view = view;
		init();
	}
	
	inline private function init() {
		GlobalEventDispatcher.getInstance().eventDispatcher.addEventListener(GameEvent.RISK_DICE_CLICKED, RISK_DICE_CLICKED_HANDLER);
	}
	
	private function RISK_DICE_CLICKED_HANDLER(e:GameEvent) {
		//if (currentCount < totalCount) {
			//currentCount++;
			view.selectOrDeselectDice(e.data.diceNum);
		//}
		//else {
			//view.deselectDice(e.data.diceNum);
		//}
	}
}