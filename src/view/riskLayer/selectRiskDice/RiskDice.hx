package view.riskLayer.selectRiskDice;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Sprite;

/**
 * ...
 * @author Igor Skotnikov
 */
class RiskDice extends Sprite
{
	
	public var diceNum(default, null):Int;
	
	public var isSelected(default, null):Bool;
	public var isDisabled(default, null):Bool;
	
	private var selectBorderImage:Bitmap;
	private var winBorderImage:Bitmap;
	private var imageNormal:Bitmap;
	private var imageDisabled:Bitmap;
	
	private var mediator:RiskDiceMediator;
	
	public function new(diceNum:Int, winBorderImage:BitmapData, selectBorderImage:BitmapData, imageNormal:BitmapData, imageDisabled:BitmapData) 
	{
		super();
		mediator = new RiskDiceMediator(this);
		
		this.diceNum = diceNum;
		this.winBorderImage = new Bitmap(winBorderImage);
		this.selectBorderImage = new Bitmap(selectBorderImage);
		this.imageNormal = new Bitmap(imageNormal);
		this.imageDisabled = new Bitmap(imageDisabled);
		this.selectBorderImage.x = this.selectBorderImage.y = -2;
		this.winBorderImage.x = this.winBorderImage.y = -2;
		addChild(this.imageNormal);
	}
	public function setIsSelected(value:Bool):Void {
		isSelected = value;
		if (isSelected)
			addChild(selectBorderImage);
		else
			removeChild(selectBorderImage);
	}
	public function setIsDisabled(value:Bool):Void {
		isDisabled = value;
		if (isDisabled)
			addChild(imageDisabled);
		else
			removeChild(imageDisabled);
	}
	
	public function dispose():Void {
		mediator.dispose();
		removeChildren();
	}

}