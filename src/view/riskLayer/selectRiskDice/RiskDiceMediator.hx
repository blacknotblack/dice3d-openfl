package view.riskLayer.selectRiskDice;

import events.GameEvent;
import openfl.events.MouseEvent;

/**
 * ...
 * @author Igor Skotnikov
 */
class RiskDiceMediator
{
	var view:RiskDice;
	
	public function new(view:RiskDice) 
	{
		this.view = view;
		init();
	}
	
	inline private function init() {
		view.addEventListener(MouseEvent.MOUSE_DOWN, MOUSE_HANDLER);
	}
	
	private function MOUSE_HANDLER(e:MouseEvent) {
		GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.RISK_DICE_CLICKED, {diceNum:view.diceNum}));
	}
	
	public function dispose():Void {
		view.removeEventListener(MouseEvent.MOUSE_DOWN, MOUSE_HANDLER);
	}
}