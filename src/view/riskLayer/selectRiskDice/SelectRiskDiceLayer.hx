package view.riskLayer.selectRiskDice;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import data.multiplayer.RiskModel;

/**
 * ...
 * @author Igor Skotnikov
 */
class SelectRiskDiceLayer extends Sprite
{
	private var mediator:SelectRiskDiceLayerMediator;

	private var dice:Array<RiskDice>;
	//private var dice2:RiskDice;
	//private var dice3:RiskDice;
	//private var dice4:RiskDice;
	//private var dice5:RiskDice;
	//private var dice6:RiskDice;
	//
	private var totalCount(default, never):Int = 3;
	private var currentCount:Int = 0;
	
	public function new()
	{
		super();
		mediator = new SelectRiskDiceLayerMediator(this);
		dice = [];
		dice[0] = new RiskDice(1, GameAssets.getInstance().RISC_DICE_WIN_BORDER, GameAssets.getInstance().RISC_DICE_SELECT_BORDER, GameAssets.getInstance().RISK_DICE1, GameAssets.getInstance().RISK_DICE_DISABLED);
		dice[0].x = 0;
		dice[0].y = 0;
		addChild(dice[0]);
		dice[1] = new RiskDice(2, GameAssets.getInstance().RISC_DICE_WIN_BORDER, GameAssets.getInstance().RISC_DICE_SELECT_BORDER, GameAssets.getInstance().RISK_DICE2, GameAssets.getInstance().RISK_DICE_DISABLED);
		dice[1].x = 70;
		dice[1].y = 0;
		addChild(dice[1]);
		dice[2] = new RiskDice(3, GameAssets.getInstance().RISC_DICE_WIN_BORDER, GameAssets.getInstance().RISC_DICE_SELECT_BORDER, GameAssets.getInstance().RISK_DICE3, GameAssets.getInstance().RISK_DICE_DISABLED);
		dice[2].x = 140;
		dice[2].y = 0;
		addChild(dice[2]);
		dice[3] = new RiskDice(4, GameAssets.getInstance().RISC_DICE_WIN_BORDER, GameAssets.getInstance().RISC_DICE_SELECT_BORDER, GameAssets.getInstance().RISK_DICE4, GameAssets.getInstance().RISK_DICE_DISABLED);
		dice[3].x = 0;
		dice[3].y = 70;
		addChild(dice[3]);
		dice[4] = new RiskDice(5, GameAssets.getInstance().RISC_DICE_WIN_BORDER, GameAssets.getInstance().RISC_DICE_SELECT_BORDER, GameAssets.getInstance().RISK_DICE5, GameAssets.getInstance().RISK_DICE_DISABLED);
		dice[4].x = 70;
		dice[4].y = 70;
		addChild(dice[4]);
		dice[5] = new RiskDice(6, GameAssets.getInstance().RISC_DICE_WIN_BORDER, GameAssets.getInstance().RISC_DICE_SELECT_BORDER, GameAssets.getInstance().RISK_DICE6, GameAssets.getInstance().RISK_DICE_DISABLED);
		dice[5].x = 140;
		dice[5].y = 70;
		addChild(dice[5]);
	}
	
	public function selectOrDeselectDice(diceNum:Int):Void {
		if (dice[diceNum - 1].isSelected) {
			dice[diceNum - 1].setIsSelected(false);
			for (i in 0...6) if (dice[i].isDisabled) dice[i].setIsDisabled(false);
			currentCount--;
			RiskModel.getInstance().removeDice(diceNum);
		}
		else {
			if (currentCount < totalCount && !dice[diceNum - 1].isSelected) {
				currentCount++;
				dice[diceNum - 1].setIsSelected(true);
				RiskModel.getInstance().addDice(diceNum);
			}
			if (currentCount == totalCount) {
				for (i in 0...6) if (!dice[i].isSelected) dice[i].setIsDisabled(true);
			}
		}
	}
}