package view.riskLayer;

import openfl.display.Sprite;
import view.gameLayer.buttonsLayer.rollButton.RollDiceButton;
import view.riskLayer.buttons.BackGameButton;
import view.riskLayer.selectRiskDice.SelectRiskDiceLayer;

/**
 * ...
 * @author Igor Skotnikov
 */
class RiskLayer extends Sprite
{
	private var riskLayerMediator:RiskLayerMediator;
	
	private var selectRiskDice:SelectRiskDiceLayer;
	private var rollButton:RollDiceButton;
	private var backGameButton:BackGameButton;

	public function new() 
	{
		super();
		y = 40;
		selectRiskDice = new SelectRiskDiceLayer();
		addChild(selectRiskDice);
		
		rollButton = new RollDiceButton();
		rollButton.x = 200;
		rollButton.y = 400;
		addChild(rollButton);
		
		backGameButton = new BackGameButton();
		backGameButton.x = 300;
		backGameButton.y = 400;
		addChild(backGameButton);
	}
	
}