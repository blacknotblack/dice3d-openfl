package view.riskLayer.buttons;

import data.const.AppCommands;
import data.multiplayer.RiskModel;
import openfl.events.MouseEvent;
import events.GameEvent;

/**
 * ...
 * @author Igor Skotnikov
 */
class BackGameButtonMediator
{
	
	private var view:BackGameButton;
	public function new(view:BackGameButton) 
	{
		this.view = view;
		view.addEventListener(MouseEvent.MOUSE_DOWN, MOUSE_HANDLER);
	}
	
	private function MOUSE_HANDLER(e:MouseEvent) {
		
		GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.HIDE_RISK_GAME));
	}
	
	public function dispose():Void {
		view.removeEventListener(MouseEvent.MOUSE_DOWN, MOUSE_HANDLER);
	}
	
}