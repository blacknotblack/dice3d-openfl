package view.riskLayer.buttons;

import govnoknopka.skin.SkinButton;
import openfl.Assets;
import openfl.display.Bitmap;

/**
 * ...
 * @author Igor Skotnikov
 */
class RollDiceButtonSkin extends SkinButton
{
	
	public function new() 
	{
		super();
		skinOver = new Bitmap(Assets.getBitmapData("assets/img/control/BigBtnOver.png"));
		skinUp = new Bitmap(Assets.getBitmapData("assets/img/control/BigBtnUp.png"));
		skinDown = new Bitmap(Assets.getBitmapData("assets/img/control/BigBtnDown.png"));
	}
	
}