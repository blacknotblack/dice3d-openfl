package view.riskLayer.buttons;

import data.const.AppCommands;
import data.multiplayer.RiskModel;
import openfl.events.MouseEvent;
import service.socket.SocketServiceEvent;

/**
 * ...
 * @author Igor Skotnikov
 */
class RollDiceButtonMediator
{
	
	private var view:RollDiceButton;
	public function new(view:RollDiceButton) 
	{
		this.view = view;
		view.addEventListener(MouseEvent.MOUSE_DOWN, MOUSE_HANDLER);
	}
	
	private function MOUSE_HANDLER(e:MouseEvent) {
		
		GlobalEventDispatcher.getInstance().dispatch(new SocketServiceEvent(SocketServiceEvent.SENDING_PACKET, AppCommands.MULTIPLAYER_ROLL_RISK, {"dices":RiskModel.getInstance().dices} ));
	}
	
	public function dispose():Void {
		view.removeEventListener(MouseEvent.MOUSE_DOWN, MOUSE_HANDLER);
	}
	
}