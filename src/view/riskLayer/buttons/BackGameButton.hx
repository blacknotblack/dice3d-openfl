package view.riskLayer.buttons;
import govnoknopka.button.Button;
import openfl.geom.Rectangle;
import openfl.text.TextFormat;

/**
 * ...
 * @author Igor Skotnikov
 */
class BackGameButton extends Button
{
	
	public var mediator:BackGameButtonMediator;
	public function new() 
	{
		super(new BackGameButtonSkin(), new Rectangle(0,0,800,480), new TextFormat(GameAssets.getInstance().font.fontName, 20, 0x1FF505));
		buttonMode = true;
		setSize(80, 40);
		text = Localization.getInstance().data.backGameButtonLabel;
		
		mediator = new BackGameButtonMediator(this);
	}
	
	public function dispose():Void {
		mediator.dispose();
		mediator = null;
	}
	
}