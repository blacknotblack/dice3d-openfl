package view.riskLayer;

import openfl.events.EventDispatcher;
import openfl.display.Sprite;
import openfl.events.Event;


/**
 * ...
 * @author Igor Skotnikov
 */
class RiskLayerMediator
{
	/* VARS */
	private var riskLayer:RiskLayer;
	
	
	//-- inject --//
	//private var room:Room;
	private var eventDispatcher:EventDispatcher;
	private var contextview:Sprite;
	
	/* CONSTRUCTOR */
	public function new(riskLayer:RiskLayer) 
	{
		this.riskLayer = riskLayer;
		
		//room = Room.getInstance();
		contextview = Context.getInstance().contextview;
		
		setupListeners();
		//setupAll();
	}
	
	private function setupListeners():Void {
		eventDispatcher = GlobalEventDispatcher.getInstance().eventDispatcher;
		
		//contextview.stage.addEventListener(Event.RESIZE, RESIZE_HANDLER);
	}
	
	/* HANDLERS */
	
	
	private function RESIZE_HANDLER(e:Event):Void {
		var factorX:Float = contextview.stage.stageWidth / Main.WIDTH;
		var factorY:Float = contextview.stage.stageHeight / Main.HEIGHT;
		var factor:Float = (factorX < factorY)?factorX:factorY;
	}
	
	
	/* API */
	
}