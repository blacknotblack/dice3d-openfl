package;

import haxe.Json;
import openfl.errors.Error;

/**
 * ...
 * @author Igor Skotnikov
 */
class AppData
{
	
	private static var _instance = new AppData();
	
	public var host(default, null)			:String = "10.44.2.22"; // 136.243.19.226 5.9.183.210
	public var port(default, null)			:UInt 	= 2000;
	public var p (default, null)			:String = "9077c97d490c70fb8c3e9250eaf5f1ac"; //eb42ece372be1779ec4454f93dc2614a  5d203e0128e22aba1c1641e3c29bc275 41ec865a8ef2514a39ae2840a01c68fb fcea30c1012d4c2d74b9eeb691c74e8a
	public var r(default, null)				:Int 	= 1; //86115
	public var currency(default, null)		:String = "USD";
	public var debug(default, null)			:Int 	= 1;
	
	public var over(default, null)			:Int 	= 1;
	public var under(default, null)			:Int 	= 1;
	public var equal(default, null)			:Int 	= 1;
	public var double(default, null)		:Int 	= 1;
	public var alldoubles(default, null)	:Int 	= 1;
	public var autoroll(default, null)		:Int 	= 1;

	public function new() 
	{
		if (_instance != null)  throw new Error("Must be called throw getInstance()");
	}
	
	public static function getInstance():AppData {
		return _instance;
	}
	
	public function parseData(params:Dynamic):Void {
		var fields:Array<String> = Reflect.fields(this);
		for (field in fields) {
			if(Reflect.hasField(params, field)){
            	Reflect.setField(this, field, cast(Reflect.getProperty(params, field)));
        	}
		}
	}
}