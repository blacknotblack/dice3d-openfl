package events;
import openfl.events.Event;

/**
 * ...
 * @author Igor Skotnikov
 */
class GameEvent extends Event
{

	inline public static var GAME_TYPE_BUTTON_SELECTED:String = 'gameTypeButtonSelected';
	inline public static var STAT_TYPE_BUTTON_SELECTED:String = 'statTypeButtonSelected';
	
	//inline public static var UP_BUTTON_PRESSED:String = 'upButtonPressed';
	//inline public static var DOWN_BUTTON_PRESSED:String = 'downButtonPressed';
	inline public static var ARROW_BUTTON_PRESSED:String = 'arrowdButtonPressed';
	
	inline public static var UPDATE_MULTIPLICATOR:String = "updateMultiplicator";
	inline public static var UPDATE_MYSTATS:String = "updateMyStats";
	
	inline public static var DRAW_TRAECTORY:String = "drawTraectory";
	
	
	inline public static var RISK_DICE_CLICKED:String = "riskDiceClicked";
	inline public static var SHOW_RISK_GAME:String = "showRiskGame";
	inline public static var HIDE_RISK_GAME:String = "hideRiskGame";
	
	public var data:Dynamic;

	public function new(type:String, data:Dynamic=null, bubbles:Bool=false, cancelable:Bool=false){
		super(type, bubbles, cancelable);
		this.data = data;
	}

	override public function clone():Event{
		return new ApplicationEvent(type, data, bubbles, cancelable);
	}
	
}