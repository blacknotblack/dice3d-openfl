package events;
import openfl.events.Event;

/**
 * ...
 * @author Igor Skotnikov
 */
class ApplicationEvent extends Event
{

	inline public static var ASSETS_LOADED:String = 'assetsLoaded';
	inline public static var RESOURCE_LOAD:String = 'resourceLoad';
	inline public static var SETUP_CONNECTION:String = 'setupConnection';
	inline public static var STARTUP_COMPLETE:String = 'startupComplete';
	inline public static var RESIZE:String = 'resize';
	inline public static var FULL_SCREEN:String = 'fullSreen';

	public var data:Dynamic;

	public function new(type:String, data:Dynamic=null, bubbles:Bool=false, cancelable:Bool=false){
		super(type, bubbles, cancelable);
		this.data = data;
	}

	override public function clone():Event{
		return new ApplicationEvent(type, data, bubbles, cancelable);
	}
	
}