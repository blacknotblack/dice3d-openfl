package;

import openfl.errors.Error;

/**
 * ...
 * @author Igor Skotnikov
 */
class Localization
{

	/* VARS */
	private static var _instance:Localization = new Localization();
	
	public var data:Dynamic;
	
	public function new() 
	{
		if (_instance != null)  throw new Error("Must be called throw getInstance()");
		else {
			data = {
				rollButtonLabel: "Roll",
				backGameButtonLabel: "Collect",
				
				underGameTypeButton: "under",
				overGameTypeButton: "over",
				equalGameTypeButton: "equal",
				doubleGameTypeButton: "double",
				allDoubleGameTypeButton: "any doubles",
				
				myBetsButtonLabel: "My bets",
				allBetsButtonLabel: "All bets",
				
				// TOP BAR
				leaveRoomButtonLabel:"Leave Room",
				bankLabel:"Bank",
				balanceLabel:"User Balance",
				gameIDLabel:"Game ID:",
				
				// Tooltips
				settingsButtonTooltip:"Settings",
				restartButtonTooltip:"Restart the Game",
				leaveRoomButtonTooltip:"Return to lobby",
				soundButtonTooltip:"Sound ON/OFF",
				screenButtonTooltip:"Fullscreen ON/OFF"
			}
		}
	}
	
	public function parseData(params:Dynamic):Void {
		var fields:Array<String> = Reflect.fields(data);
		for (field in fields) {
			if(Reflect.hasField(params, field)){
            	Reflect.setField(data, field, Reflect.getProperty(params, field));
        	}
		}
	}
	
	public static function getInstance():Localization {	
		return _instance;
	}
}