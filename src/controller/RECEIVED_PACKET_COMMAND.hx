package controller;

import events.GameEvent;
import haxe.Json;
import service.socket.SocketServiceEvent;
import service.socket.Packet;
import data.const.AppCommands;
import data.multiplayer.Room;

/**
 * ...
 * @author Igor Skotnikov
 */
class RECEIVED_PACKET_COMMAND{

	/* VARS */
	public var userID			:UInt;
	public var command			:UInt;
	public var data				:Dynamic;
	
	//-- inject --//
	private var room			:Room;
	
	/* CONSTRUCTOR */
	public function new() 
	{
		room = Room.getInstance();
		GlobalEventDispatcher.getInstance().eventDispatcher.addEventListener(SocketServiceEvent.RECEIVED_PACKET,	RECEIVED_PACKET_HANDLER);
	}
	
	/* API */
	public function RECEIVED_PACKET_HANDLER(e:SocketServiceEvent) {
		userID 	= e.packet.userID;
		command = e.packet.command;
		data	= e.packet.data;
		
		if(!(command==AppCommands.SYSTEM_PING) && AppData.getInstance().debug == 1) trace("RECEVED: " + e.packet);
		if (data!=null){
			
		}
		
		switch(command) {
			case AppCommands.MULTIPLAYER_PLAYER_ADD:
				if (Reflect.hasField(data, "player")) {
					room.player.createPlayer(data.player.id, data.player.name, data.player.rating, data.player.balance);
				}
				if (Reflect.hasField(data, "multiplicator") && Reflect.hasField(data, "minBet") && Reflect.hasField(data, "maxBet")) {
					room.createRoom(data.minbet, data.maxBet, data.multiplicator);
					GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.UPDATE_MULTIPLICATOR));
				}
			case AppCommands.MULTIPLAYER_CASINO_DICE:
				if (Reflect.hasField(data, "game")) {
					GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.UPDATE_MYSTATS, data.game));
				}
				if (Reflect.hasField(data, "traectory")) {
					GlobalEventDispatcher.getInstance().dispatch(new GameEvent(GameEvent.DRAW_TRAECTORY, data.traectory));
				}
				if (Reflect.hasField(data, "gameId")) {
					
				}
			case AppCommands.SYSTEM_PING:
				GlobalEventDispatcher.getInstance().dispatch(new SocketServiceEvent(SocketServiceEvent.SENDING_PACKET, AppCommands.SYSTEM_PING));
		}
	}
}
