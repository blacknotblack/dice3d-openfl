package service.socket;

#if html5
import js.html.ArrayBuffer;
import js.html.Uint8Array;
#end
import haxe.crypto.Md5;
import haxe.Json;
import openfl.errors.Error;
import openfl.utils.ByteArray;

/**
 * ...
 * @author Igor Skotnikov
 */
class Packet{
	/* VARS */
	public var userID	:Int;
	public var command	:Int;
	public var data		:Dynamic;
	public var valid	:Bool;

	/* CONSTRUCTOR */
	public function new(){
	}
	
	/* API */
	#if html5
	public function getArrayBufferView():Uint8Array{
		var json:String = (data==null)?"{}":Json.stringify(data);
		//var sign:String = Md5.encode(""+userID+command);

		var buf = new ArrayBuffer(4+1+4+json.length); // 2 bytes for each char
		var bufView = new Uint8Array(buf);
		
		//trace(sign.length, json.length);
		
		var offset:Int = 0;
		bufView.set(intToUint8Array(userID));
		offset += 4;
		bufView.set(byteToUint8Array(command), offset);
		offset += 1;
		bufView.set(intToUint8Array(json.length),offset);
		offset += 4;
		for (i in 0...json.length) {
			bufView[i+offset] = json.charCodeAt(i);
		}
		offset +=  json.length;
		//bufView.set(intToUint8Array(sign.length), offset);
		//offset += 4;
		//for (i in 0...sign.length) {
			//bufView[i+offset] = sign.charCodeAt(i);
		//}
		
		return bufView;
	}
	
	public static function intToUint8Array(x:Int):Uint8Array {
		var buf = new ArrayBuffer(4);
		var bufView = new Uint8Array(buf);
		bufView[0] = (x >> 24) & 0xFF;
		bufView[1] = (x >> 16) & 0xFF;
		bufView[2] = (x >> 8) & 0xFF;
		bufView[3] = x & 0xFF;
		return bufView;
	}
	public static function byteToUint8Array(x:Int):Uint8Array {
		var buf = new ArrayBuffer(1);
		var bufView = new Uint8Array(buf);	
		bufView[0] = x;
		return bufView;
	}
	
	public static function createPacketByArrayBuffer(buf:ArrayBuffer):Packet{
		var packet:Packet = new Packet();
		
		var bufView:Uint8Array = new Uint8Array(buf);
		
		var byteArray:ByteArray = new ByteArray();
		for (i in 0...bufView.byteLength) {
			byteArray.writeByte(bufView[i]);
		}
		
		byteArray.position 	= 0;
		packet.userID 		= byteArray.readUnsignedInt();
		packet.command 		= byteArray.readByte();
		
		var jsonLength:Int = byteArray.readUnsignedInt();
		var json:String = byteArray.readUTFBytes(jsonLength);
		try{
			packet.data = Json.parse(json);
		}
		catch(e:Dynamic) {
			throw new Error("Parsing Data Error");
		}
		//var signLength:Int = byteArray.readUnsignedInt();
		//var sign:String	= byteArray.readUTFBytes(signLength);
		//packet.checkSign(sign);
		//
		return packet;
	}
	public static function createPacketByConstructor(userID:Int, command:Int, data:Dynamic):Packet{
		var packet:Packet = new Packet();
		
		packet.userID = userID;
		packet.command = command;
		packet.data = data;
		//packet.checkSign(Md5.encode(""+packet+packet.command));
		
		return packet;
	}
	#end
	public function checkSign(sign:String):Void{
		if(sign == Md5.encode(""+userID+command)) valid = true;
	}
	public function toString(full:Bool = true):String{
		return (full ? '( USERID : '+userID+', ' : '(') + 'COMMAND : '+command+', DATA : '+Json.stringify(data)+ (full ? ', SIGN : '+Md5.encode(""+userID+command)+')' : ')');
	}
}