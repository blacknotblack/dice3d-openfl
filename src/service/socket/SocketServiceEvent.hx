package service.socket;

/**
 * ...
 * @author Igor Skotnikov
 */
import openfl.events.Event;
	
import service.socket.Packet;

class SocketServiceEvent extends Event{
	/* CONSTANTS */
	inline public static var SUCCESSFUL_CONNECTION	:String = 'successfulConnection';
	inline public static var FATAL_ERROR			:String = 'fatalError';
	inline public static var SENDING_PACKET			:String = 'sendingPacket';
	inline public static var RECEIVED_PACKET		:String = 'receivedPacket';
	/* VARS */
	public var command:UInt;
	public var data:Dynamic;
	public var packet:Packet;
	/* CONSTRUCTOR */
	public function new(type:String, command:UInt=0, data:Dynamic=null, packet:Packet=null, bubbles:Bool=false, cancelable:Bool=false){
		super(type, bubbles, cancelable);
		this.command = command;
		this.data = data;
		this.packet = packet;
	}
	/* API */
	override public function clone():Event{
		return new SocketServiceEvent(type, command, data, packet, bubbles, cancelable);
	}
}