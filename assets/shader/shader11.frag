precision mediump float;

varying vec2 vTextureCoord;
varying vec3 vFragPosition;
uniform sampler2D uSampler;
uniform sampler2D uNormalSampler;
uniform vec4 sphereColor;

varying mat3 TBN;

void main(void) {	
	/*
	vec4 textureColor = texture2D(uSampler, vTextureCoord);	 
	textureColor = vec4(textureColor.rgb, textureColor.a); 
	 
    vec3 normal = texture2D(uNormalSampler, vTextureCoord).rgb * 2.0 - 1.0;
    normal = normalize(TBN * normalize(normal));

    // Diffuse
    vec3 lightDir = normalize(vec3(0.0, 10.0, 0.0));
    float diff = max(dot(lightDir, normal), 0.0);
    vec4 diffuse = diff * vec4(0.2, 0.2, 0.2, 1.0);

	 // Specular
    vec3 viewDir = normalize(vec3(0.0, 10.0, 0.0));
    vec3 reflectDir = reflect(-lightDir, normal);  
    float spec = max(dot(viewDir, reflectDir), 0.0);
	
	gl_FragColor = textureColor + diffuse;//vec4(diffuse, 1.0);//mix(sphereColor, textureColor, mask);	 
	
	*/
	vec3 normal = texture2D(uNormalSampler, vTextureCoord).rgb * 2.0 - 1.0;
    //normal = normalize(normal);
	normal = normalize(TBN * normalize(normal));
	
	vec3 color = texture2D(uSampler, vTextureCoord).rgb;
	vec3 ambient = 0.1*color;
	
	vec3 lightDir = normalize( vec3(200, 1000, 0) -  vFragPosition);
	float diff = max(dot(lightDir, normal), 0.0);
	vec3 diffuse = diff * color;
	
	vec3 viewDir = normalize(vec3(60, 75, -30)- vFragPosition);
	vec3 reflectDir = reflect(-lightDir, normal);
	vec3 halfwayDir = normalize(lightDir + viewDir);
	float spec = pow(max(dot(normal, halfwayDir), 0.0), 32.0);
    vec3 specular = vec3(0.35) * spec;
    
    gl_FragColor = vec4(ambient + diffuse + specular, 1.0);
}